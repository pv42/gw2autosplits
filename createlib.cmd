prompt $G
dumpbin /exports d3d9.dll > exports.txt
echo LIBRARY D3D9 > d3d9.def
echo EXPORTS >> d3d9.def
@echo off
for /f "skip=19 tokens=4" %%A in (exports.txt) do echo %%A >> d3d9.def
@echo on
lib /def:d3d9.def /out:d3d9.lib /machine:x64
prompt $P$G