

#ifndef _GW2AutoSplits_H
#define _GW2AutoSplits_H

#include <vector>
#include <string>
#include <chrono>
#include <filesystem>

#include "UI.h"
#include "arcdps.h"
#include "MumbleAPI.h"
#include "GW2.h"
#include "Splits.h"
#include "timing.h"


using std::string;
using std::filesystem::path;
using std::vector;
using std::shared_ptr;
using std::unique_ptr;

class GW2AutoSplits {

public:
	static unique_ptr<GW2AutoSplits> gw2autoSplits;
	static arcdps_exports* s_init();
	static uintptr_t s_release();
	static uintptr_t s_imgui(uint32_t not_charsel_or_loading);
	static void arc_log(string str);
	static void s_uiOptions();
	static const path getSplitsPath();
	GW2AutoSplits(char* arcversionstr);
	arcdps_exports* Init();
	uintptr_t Release();
protected:
	/** combat callback -- may be called asynchronously. return ignored
	  *  one participant will be party/squad, or minion of. no spawn statechange events. despawn statechange only on marked boss npcs */
	uintptr_t CombatEvent(cbtevent* ev, ag* src, ag* dst, char* skillname, uint64_t id, uint64_t revision);
	/** ??? */
	uintptr_t CombatEventLocal(cbtevent* ev, ag* src, ag* dst, char* skillname, uint64_t id, uint64_t revision);
	/* window callback -- return is assigned to umsg (return zero to not be processed by arcdps or game) */
	uintptr_t WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	/* ingui draw call */
	uintptr_t ImGui(uint32_t not_charsel_or_loading);
private:
	static bool crashed;
	string arcVersion;
	arcdps_exports arc_exports;
	MumbleApi mumbleApi;
	ourclock::time_point start;
	UI ui;
	vector<shared_ptr<SplitSet>> splitSets;
	vector<shared_ptr<ActiveSplitSet>> unsafedSplitSets;
	shared_ptr<ActiveSplitSet> activeSplitSet;
	void logSplit(const string& name, ourclock::time_point time);
	void eventLog(cbtevent* event, ag* src, ag* dst, char* skillname, uint64_t id, uint64_t revision);
	bool selectSplitSet(int mapId);
	void loadAllSplitFiles();
	void loadSplitSet(const string& filename);
	static intptr_t s_wnd_proc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	static uintptr_t s_combat(cbtevent* ev, ag* src, ag* dst, char* skillname, uint64_t id, uint64_t revision);
	static uintptr_t s_combat_local(cbtevent* ev, ag* src, ag* dst, char* skillname, uint64_t id, uint64_t revision);
	static bool s_is_init();
};

#endif // _GW2AutoSplits_H






