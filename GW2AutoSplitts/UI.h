#ifndef _UI_H
#define _UI_H

#include <string>
#include <chrono>
#include <vector>

#include "imgui/imgui.h"

#include "Splits.h"
#include "MumbleApi.h"
#include "timing.h"
#include "Settings.h"

using std::string;
using std::vector;


class SplitEditorUI {
public:
	SplitEditorUI();
	SplitEditorUI(const SplitSet&);
	void drawSplitEditor(const std::shared_ptr<ActiveSplitSet> activeSet, bool* showEditor, const MumbleApi& mumbleApi);
private:
	void drawSplitCondition(vector<SplitCondition_PTR>& conditions, int index, int imguiIdOffset = 0);
	const static int NAME_BUFF_SIZE = 64;
	vector<int> mapIdBuff;
	char nameBuff[NAME_BUFF_SIZE];
	vector<vector<int>> splitConditionTypes;
	bool editingOthermaps;
#define EDIT_OTHER_MAPS_BUFSIZE 512
	char editOtherMapsBuff[EDIT_OTHER_MAPS_BUFSIZE];
};

class UI {
public:
	UI();
	void drawSettings();
	void draw(int mapId, float* pos, milliseconds time, std::shared_ptr<ActiveSplitSet> activeSet, std::vector<std::shared_ptr<ActiveSplitSet>>& unsafedSplitSets, const MumbleApi& mumbleApi);
	void options();
	uintptr_t WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
private:
	void drawMain(int mapId, float* pos, milliseconds time, std::shared_ptr<ActiveSplitSet> activeSet, std::vector<std::shared_ptr<ActiveSplitSet>>& unsafedSplitSets);
	void drawSplit(Split& split, bool isActive, milliseconds currentTime, const std::shared_ptr <ActiveSplitSet> activeSet, int splitIndex);
	void drawActiveSet(milliseconds currentTime, std::shared_ptr<ActiveSplitSet>& activeSet, float height);
	void InputKey(const char* label, Key* key, arc_config& cf);
	bool shouldReset;
	bool shouldSplit;
	bool shouldSkip;
	bool shouldUnsplit;
	bool showMain;
	bool showSettings;
	bool showEditor;
	SplitEditorUI splitEditor;
	int lastSplitIndex;
	// keyIn
	const char* currentInputKeyLabel;
	int* currentInputKeyTarget;
};


#endif // !_UI_H