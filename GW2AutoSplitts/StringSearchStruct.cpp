#include "StringSearchStruct.h"
#include "string_format.hpp"
#include "logger.h"
#include <algorithm>
#include <cctype>
#include <string>

int getCharIndex(char c) {
	if ('A' <= c && c <= 'Z') {
		return  1 + c - 'A';
	} else if ('a' <= c && c <= 'z') {
		return 1 + c - 'a';
	}
	return 0;
}

StringSearchStruct::StringSearchStruct() {
	for (int i = 0; i < 27; i++) {
		data.push_back(vector<vector<pair<uint64_t, string>>>());
		for (int j = 0; j < 27; j++) {
			data[i].push_back(vector<pair<uint64_t, string>>());
		}
	}
}

void StringSearchStruct::addElement(string str, const uint64_t i) {
	int index0, index1;
	if (str.length() == 0) {
		index0 = 0;
		index1 = 0;
	} else {
		index0 = getCharIndex(str.at(0));
		if (str.length() == 1) {
			index1 = 0;
		} else {
			index1 = getCharIndex(str.at(1));
		}
	}
	data[index0][index1].push_back(pair<uint64_t, string>(i, str));
}

string toLower(const string str) {
	string lower = str; // this allocates the space
	std::transform(str.begin(), str.end(), lower.begin(),
		[](unsigned char c) { return std::tolower(c); });
	return lower;
}

vector<pair<uint64_t, string>> StringSearchStruct::search(const string searchStr, int limit) const {
	vector<pair<uint64_t, string>> results;
	if (searchStr.size() >= 2) {
		int index0 = getCharIndex(searchStr.at(0));
		int index1 = getCharIndex(searchStr.at(1));
		string searchLwr = toLower(searchStr);
		for (pair<uint64_t, string> p : data[index0][index1]) {
			if (toLower(p.second).rfind(searchLwr, 0) == 0) {
				results.push_back(p);
				if (results.size() >= limit) {
					return results;
				}
			}
		}
		return results;
	} else if (searchStr.size() == 1) {
		int index0 = getCharIndex(searchStr.at(0));
		for (int index1 = 0; index1  < 27; index1++) {
			for (pair<uint64_t, string> p : data[index0][index1]) {
				results.push_back(p);
				if (results.size() >= limit) return results;
			}
		}
		return results;
	} else {
		// size is 0, just return the first elements
		for (int index0 = 0; index0 < 27; index0++) {
			for (int index1 = 0; index1 < 27; index1++) {
				for (pair<uint64_t, string> p : data[index0][index1]) {
					results.push_back(p);
					if (results.size() >= limit) return results;
				}
			}
		}
		return results;
	}
}