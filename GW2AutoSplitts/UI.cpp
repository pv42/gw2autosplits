#include <string>
#include <chrono>
#include <array>
#include <set>

#include "UI.h"
#include "imgui/imgui.h"
#include "GW2AutoSplits.h"
#include "GW2.h"
#include "logger.h"
#include "string_format.hpp"
#include "Settings.h"
#include "Widgets.h"


using std::chrono::milliseconds;
using std::chrono::seconds;
using std::chrono::minutes;
using std::reinterpret_pointer_cast;
using std::vector;
using std::shared_ptr;
using std::array;
using std::make_shared;
using std::set;

UI::UI() : splitEditor() {
	showMain = true;
	showSettings = false;
	showEditor = false;
	lastSplitIndex = -1;
	Logger::d("[UI] UI initialized");
}

void UI::options() {
	if (ImGui::BeginMenu("AutoSplits##uid900")) {
		ImGui::Checkbox("show##uid901", &showMain);
		ImGui::Checkbox("settings##uid902", &showSettings);
		ImGui::Checkbox("editor", &showEditor);
		ImGui::EndMenu();
	}
}

SplitEditorUI::SplitEditorUI() : nameBuff("") {
	// nothing
}

SplitEditorUI::SplitEditorUI(const SplitSet& set) : nameBuff("") {
	Logger::w("used sp_ui cpy constr");
}


void drawSpeciesSelector(const char* label, uint64_t* currentValue, char* search_str, size_t search_str_size) {
	char preview_value[64];
	sprintf_s(preview_value, "%s (%llu)", SpeciesIdStorage::getNameById(*currentValue).c_str(), *currentValue);
	if (ImGui::BeginCombo(label, preview_value)) {
		char searchFieldId[64];
		sprintf_s(searchFieldId, "%s_itxt_search", label);
		ImGui::InputText(searchFieldId, search_str, search_str_size);
		
		char disp[64];
		// if input is int display option for it too
		if (atoll(search_str) > 0) {
			bool selected = false;
			long long id = atoll(search_str);
			sprintf_s(disp, "%s (%llu)", SpeciesIdStorage::getNameById(id).c_str(), id);
			ImGui::Selectable(disp, &selected);
			if (selected) {
				*currentValue = id;
			}
		}
		vector<pair<uint64_t, string>> searchResult = SpeciesIdStorage::searchByName(search_str);
		for (pair<uint64_t, string> p : searchResult) {
			bool selected = false;
			sprintf_s(disp, "%s (%llu)", p.second.c_str(), p.first);
			ImGui::Selectable(disp, &selected);
			if (selected) *currentValue = p.first;
		}
		ImGui::EndPopup();
	}
}

static char searchStr[64];

void SplitEditorUI::drawSplitCondition(vector<SplitCondition_PTR>& conditions, int index, int imguiIdOffset) {	
	const SplitCondition_PTR& condition = conditions[index];
	const static char* typeDesc[] = {
		"Box Position",
		"Sphere Position",
		"Combat Event",
		"Buff Applied",
		"Left Combat",
		"Log Start",
		"Log End"
	};
	const static char* types[] = {
		"position",
		"sphere",
		"combat",
		"buffApplied",
		"leftCombat",
		"logStart",
		"logEnd"
	};
	int type = -1;
	
	for (int i = 0; i < 7; i++) {
		if (!condition->getType().compare(types[i])) type = i;
	}
	if(type == -1) {
		Logger::w("[UI] invalid split type:" + condition->getType());
	}
	int initialType = type;
	ImGui::Text("Type:");
	ImGui::SameLine();
	if (type != -1) {
		ImGui::Text(typeDesc[type], 4);
	} else {
		ImGui::Text("?");
	}
	//Logger::d("updated condi type");
		//ImGui::Text("/!\\type updated/!\\");
		//return;
	static char targetStr[64];
	switch (type) {
	case 0: // position
		ImGui::InputFloat3(string_format("lower limits##if3ll%d", index + imguiIdOffset).c_str(), reinterpret_pointer_cast<BoxPositionalSplitCondition>(condition)->lwr);
		ImGui::InputFloat3(string_format("upper limits##if3ul%d", index + imguiIdOffset).c_str(), reinterpret_pointer_cast<BoxPositionalSplitCondition>(condition)->upr);
		break;
	case 1:
		ImGui::InputFloat3(string_format("center##if3cen%d", index + imguiIdOffset).c_str(), reinterpret_pointer_cast<SphericalPositionalSplitCondition>(condition)->mid);
		ImGui::InputFloat(string_format("radius##ifrad%d", index + imguiIdOffset).c_str(), &(reinterpret_pointer_cast<SphericalPositionalSplitCondition>(condition)->radius));
		break;
	case 2:
		sprintf_s(targetStr, "%s", reinterpret_pointer_cast<CombatSplitCondition>(condition)->target.c_str());
		if (ImGui::InputText(string_format("Target##ittar%d", index + imguiIdOffset).c_str(), targetStr, 64)) {
			reinterpret_pointer_cast<CombatSplitCondition>(condition)->target = targetStr;
		};
		break;
	case 3:
		sprintf_s(targetStr, "%s", reinterpret_pointer_cast<BuffAppliedSplitCondition>(condition)->target.c_str());
		if (ImGui::InputText("Target", targetStr, 64)) { // todo this id 
			reinterpret_pointer_cast<BuffAppliedSplitCondition>(condition)->target = targetStr;
		}
		ImGui::InputInt(string_format("Buff ID##iibuf%d", index + imguiIdOffset).c_str(), (int*)&(reinterpret_pointer_cast<BuffAppliedSplitCondition>(condition)->buff));
		break;
	case 4:
		sprintf_s(targetStr, "%s", reinterpret_pointer_cast<LeftCombatSplitCondition>(condition)->target.c_str());
		if (ImGui::InputText(string_format("Target##ittar%d", index + imguiIdOffset).c_str(), targetStr, 64)) {
			reinterpret_pointer_cast<LeftCombatSplitCondition>(condition)->target = targetStr;
		}
		break;
	case 5:{
		uint64_t speciesID = reinterpret_pointer_cast<LogStartSplitCondition>(condition)->species_id;
		bool useId = speciesID != 0;
		ImGui::Checkbox(string_format("Specific Target##chk_LS_ss%d", index + imguiIdOffset).c_str(), &useId);
		if (useId && speciesID == 0) { // box just checked
			reinterpret_pointer_cast<LogStartSplitCondition>(condition)->species_id = 1;
		}
		if (!useId && speciesID != 0) { // box just unchecked
			reinterpret_pointer_cast<LogStartSplitCondition>(condition)->species_id = 0;
		}
		if (useId) {
			ImGui::SameLine();
			drawSpeciesSelector(string_format("##cb_LS_spec_id%d", imguiIdOffset + index).c_str(), &(reinterpret_pointer_cast<LogStartSplitCondition>(condition)->species_id), searchStr, 64);
		}
		break;
	}
	case 6:{
		uint64_t speciesID = reinterpret_pointer_cast<LogEndSplitCondition>(condition)->species_id;
		bool useId = speciesID != 0;
		ImGui::Checkbox(string_format("Specific Target##chk_LE_ss%d", index + imguiIdOffset).c_str(), &useId);
		if (useId && speciesID == 0) { // box just checked
			reinterpret_pointer_cast<LogEndSplitCondition>(condition)->species_id = 1;
		}
		if (!useId && speciesID != 0) { // box just unchecked
			reinterpret_pointer_cast<LogEndSplitCondition>(condition)->species_id = 0;
		}
		if (useId) {
			ImGui::SameLine();
			drawSpeciesSelector(string_format("##cb_LE_spec_id%d", imguiIdOffset + index).c_str(), &(reinterpret_pointer_cast<LogStartSplitCondition>(condition)->species_id), searchStr, 64);
		}
		break;
	}
	default:
		ImGui::Text("Invalid type");
	}
	if (initialType != type) { // should not happen 
		Logger::w("updated split type");
	}
	ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(0.0, 0.6f, 0.6f));
	ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(0.0f, 0.8f, 0.8f));
	ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(0.0f, 0.9f, 0.9f));
	if (ImGui::Button(string_format("delete##delid%d", index + imguiIdOffset).c_str())) {
		conditions.erase(conditions.begin() + index);
	}
	ImGui::PopStyleColor(3);
}

void SplitEditorUI::drawSplitEditor(const shared_ptr<ActiveSplitSet> activeSet, bool* showEditor, const MumbleApi& mumbleApi) {
	if(ImGui::Begin("SplitEditor", showEditor)) {
		if (!SpeciesIdStorage::isLoading() && !SpeciesIdStorage::isLoaded()) {
			SpeciesIdStorage::init();
		}
		if (!SpeciesIdStorage::isLoaded()) {
			ImGui::Text("Loading ...");
			ImGui::SameLine();
			ImGui_ex::Spinner("spin_spleditload", ImGui::GetTextLineHeight() / 2.f, 3.f, ImGui::GetColorU32(ImGuiCol_Text));
		} else {
			if (activeSet) {
				static vector<array<char, NAME_BUFF_SIZE>> splitNameBuffs;
				if (splitNameBuffs.size() < activeSet->getSplits().size()) {
					splitNameBuffs.clear();
					for (int i = 0; i < activeSet->getSplits().size(); i++) {
						splitNameBuffs.push_back(array<char, NAME_BUFF_SIZE>());
					}
				}
				ImGui::Text("File: '%s'", activeSet->filepath.c_str());
				sprintf_s(nameBuff, "%s", activeSet->getName().c_str());
				if (ImGui::InputText("Name", nameBuff, sizeof(nameBuff))) {
					activeSet->setName(nameBuff);
				}
				if (editingOthermaps) {
					ImGui::Text("other maps:");
					ImGui::SameLine();
					ImGui::InputText("##tb_edit_other_maps", editOtherMapsBuff, EDIT_OTHER_MAPS_BUFSIZE);
					ImGui::SameLine();
					if (ImGui::Button("ok##btn_ok_edit_other_maps_btn")) {
						// todo 
						editingOthermaps = false;
						char* pch;
						char* next_token = NULL;
						pch = strtok_s(editOtherMapsBuff, " ,;", &next_token);
						set<int> found = set<int>();
						while (pch != NULL) {
							int id = strtol(pch, NULL, 10);
							if (id > 0) {
								found.insert(id);
							}
							pch = strtok_s(NULL, " ,;", &next_token);
						}
						activeSet->otherMapIds = found;
					}
				} else {
					string otherMapsStr = "-";
					bool firstOtherMap = true;
					for (int map : activeSet->otherMapIds) {
						if (firstOtherMap) {
							otherMapsStr = string_format("%d", map);
							firstOtherMap = false;
						} else {
							otherMapsStr = string_format("%s, %d", otherMapsStr.c_str(), map);
						}
					}

					ImGui::Text(string_format("other maps: %s", otherMapsStr.c_str()).c_str());
					ImGui::SameLine();
					if (ImGui::Button("edit##btn_edit_other_maps_btn")) {
						editingOthermaps = true;
						strcpy_s(editOtherMapsBuff, otherMapsStr.c_str());
					}
				}

				if (ImGui::Button("delete best times")) {
					activeSet->deleteBestTimes();
				}
				//selector (left)
				static long selected = 0;
				ImGui::BeginChild("left spedit pane", ImVec2(150, 0), true);
				if (ImGui::Button("^") && selected > 0) {
					Split down = activeSet->getSplits()[selected];
					activeSet->getSplitsEditable()[selected] = activeSet->getSplits()[selected - 1LL];
					activeSet->getSplitsEditable()[selected - 1LL] = down;
				}
				ImGui::SameLine();
				if (ImGui::Button("v") && selected < activeSet->getSplits().size() - 1) {
					Split up = activeSet->getSplits()[selected];
					activeSet->getSplitsEditable()[selected] = activeSet->getSplits()[selected + 1LL];
					activeSet->getSplitsEditable()[selected + 1LL] = up;
				}
				ImGui::SameLine();
				ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(0.0, 0.6f, 0.6f));
				ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(0.0f, 0.8f, 0.8f));
				ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(0.0f, 0.9f, 0.9f));
				if (ImGui::Button("delete")) {
					vector<Split>* splits = &activeSet->getSplitsEditable();
					splits->erase(splits->begin() + selected);
					splitNameBuffs.pop_back();
				}
				ImGui::PopStyleColor(3);
				for (int i = 0; i < activeSet->getSplits().size(); i++) {
					Split split = activeSet->getSplits()[i];
					sprintf_s(splitNameBuffs[i].data(), NAME_BUFF_SIZE, "%s", split.name.c_str());
					static char selectNameBuff[NAME_BUFF_SIZE + 3];
					sprintf_s(selectNameBuff, "%d:%s", i, split.name.c_str());
					if (ImGui::Selectable(split.name.c_str(), selected == i)) selected = i;
				}
				if (ImGui::Selectable(" + add new", false)) {
					activeSet->getSplitsEditable().push_back(Split(vector<SplitCondition_PTR>(), "new split"));
					splitNameBuffs.push_back(array<char, NAME_BUFF_SIZE>());
				}
				ImGui::EndChild();
				ImGui::SameLine();
				//split edit (right)
				ImGui::BeginGroup();
				ImGui::BeginChild("right spedit pane", ImVec2(0, -ImGui::GetTextLineHeightWithSpacing() - 8)); // Leave room for 1 line below us and some padding
				if (activeSet->getSplits().size() > selected) {
					if (ImGui::InputText("split name", splitNameBuffs[selected].data(), NAME_BUFF_SIZE)) {
						activeSet->getSplitsEditable()[selected].name = string(splitNameBuffs[selected].data());
					}
					ImGui::Separator();
					ImGui::Text("split conditions:");
					ImGui::Indent();
					for (int j = 0; j < activeSet->getSplits()[selected].getConditions().size(); j++) {
						drawSplitCondition(activeSet->getSplitsEditable()[selected].getConditionsEditable(), j);
					}
					ImGui::Unindent();
					if (activeSet->getSplits()[selected].getConditions().size() == 0) {
						ImGui::Text("<No Conditions>");
					}
					if (ImGui::Button("add box position condition")) {
						activeSet->getSplitsEditable()[selected].getConditionsEditable().push_back((SplitCondition_PTR)make_shared<BoxPositionalSplitCondition>(0, 0, 0, 0));
					}
					if (ImGui::Button("add sphere position condition")) {
						activeSet->getSplitsEditable()[selected].getConditionsEditable().push_back((SplitCondition_PTR)make_shared<SphericalPositionalSplitCondition>(0, 0, 0, 0));
					}
					if (ImGui::Button("add combat condition")) {
						activeSet->getSplitsEditable()[selected].getConditionsEditable().push_back((SplitCondition_PTR)make_shared<CombatSplitCondition>("enter target name"));
					}
					if (ImGui::Button("add log start")) {
						activeSet->getSplitsEditable()[selected].getConditionsEditable().push_back((SplitCondition_PTR)make_shared<LogStartSplitCondition>());
					}
					ImGui::SameLine();
					if (ImGui::Button("add log end")) {
						activeSet->getSplitsEditable()[selected].getConditionsEditable().push_back((SplitCondition_PTR)make_shared<LogEndSplitCondition>());
					}
					ImGui::Separator();
					ImGui::Checkbox("is check point", &(activeSet->getSplitsEditable()[selected].checkpoint));
					ImGui::Text("last checkpoint conditions:");
					ImGui::Indent();
					for (int j = 0; j < activeSet->getSplits()[selected].getCheckpointConditions().size(); j++) {
						drawSplitCondition(activeSet->getSplitsEditable()[selected].getCheckpointConditionsEditable(), j, 1000);
					}
					ImGui::Unindent();
					if (activeSet->getSplits()[selected].getCheckpointConditions().size() == 0) {
						ImGui::Text("<No Conditions>");
					}
					if (ImGui::Button("add box position condition##btabpc2")) {
						activeSet->getSplitsEditable()[selected].getCheckpointConditionsEditable().push_back((SplitCondition_PTR)make_shared<BoxPositionalSplitCondition>(0, 0, 0, 0));
					}
					if (ImGui::Button("add sphere position condition##btspc2")) {
						activeSet->getSplitsEditable()[selected].getCheckpointConditionsEditable().push_back((SplitCondition_PTR)make_shared<SphericalPositionalSplitCondition>(0, 0, 0, 0));
					}
					if (ImGui::Button("add combat condition##btacc2")) {
						activeSet->getSplitsEditable()[selected].getCheckpointConditionsEditable().push_back((SplitCondition_PTR)make_shared<CombatSplitCondition>("enter target name"));
					}
					if (ImGui::Button("add log start##btalsc2")) {
						activeSet->getSplitsEditable()[selected].getCheckpointConditionsEditable().push_back((SplitCondition_PTR)make_shared<LogStartSplitCondition>());
					}
					ImGui::SameLine();
					if (ImGui::Button("add log end##btalec2")) {
						activeSet->getSplitsEditable()[selected].getCheckpointConditionsEditable().push_back((SplitCondition_PTR)make_shared<LogEndSplitCondition>());
					}
					ImGui::EndChild();
					ImGui::BeginChild("buttons");
					if (ImGui::Button("Save")) {
						activeSet->writeToFile();
					}
				}
				ImGui::EndChild();
				ImGui::EndGroup();
			} else {
				ImGui::Text("No active splitset");
				ImGui::Separator();
				ImGui::Text("create one ...");
				static char newNameBuff[NAME_BUFF_SIZE];
				static char newFNameBuff[NAME_BUFF_SIZE];
				ImGui::InputText("name", newNameBuff, 64);
				ImGui::InputText("filename", newFNameBuff, 64);
				if (ImGui::Button("create")) {
					int mapId = mumbleApi.getMapId(); // todo
					set<int> si = set<int>();
					SplitSet set = SplitSet((GW2AutoSplits::getSplitsPath() / newFNameBuff).string(), newNameBuff, mapId, si, vector<Split>());
					set.writeToFile();
				}
				ImGui::Text("Newly created set can only be used after restarting the game.");
			}
		}
	}
	ImGui::End();
}


// checks if  the current segment is slower then the sane segment in the best run
// returns false if faster or no data
bool isSegmentWorse(const shared_ptr<ActiveSplitSet> set, int splitIndex) {
	if (splitIndex == 0) return false;
	milliseconds segmentTime = set->getSegmentTime(splitIndex);
	if (segmentTime.count() == 0) return false;
	if (set->getSplits()[splitIndex].getBestRun().count() == 0) return false;
	milliseconds bestRunSegmentTime = set->getSplits()[splitIndex].getBestRun() - set->getSplits()[splitIndex - 1].getBestRun();
	return bestRunSegmentTime < segmentTime;
}

// checks if  the current segment is faster then the sane segment in the best run
// returns false if slower or no data
bool isSegmentImprovement(const shared_ptr<ActiveSplitSet> set, int splitIndex) {
	if (splitIndex == 0) return false;
	milliseconds segmentTime = set->getSegmentTime(splitIndex);
	if (segmentTime.count() == 0) return false;
	if (set->getSplits()[splitIndex].getBestRun().count() == 0) return false;
	milliseconds bestRunSegmentTime = set->getSplits()[splitIndex].getBestRun() - set->getSplits()[splitIndex - 1].getBestRun();
	return bestRunSegmentTime > segmentTime;
}

// determisns whether this 
bool isBestSegment(const Split split, const shared_ptr <ActiveSplitSet> activeSet, int splitIndex) {
	milliseconds segment = activeSet->getSegmentTime(splitIndex);
	milliseconds best = split.getBestSegment();
	return best.count() != 0 && segment.count() != 0 && segment < best;
}

// checks if after the current segemnt the total time is better
// return false if no data or time is worse
bool isImpovement(Split split, milliseconds time) {
	if (time.count() == 0 || !split.hasBestRun()) return false;
	return time < split.getBestRun();
}

// checks if after the current segemnt the total time is better
// return false if no data or time is better
bool isWorse(Split split, milliseconds time) {
	if (time.count() == 0 || !split.hasBestRun()) return false;
	return time > split.getBestRun();
}


bool shouldShowDelta(milliseconds currentTime, int splitIndex, const vector<milliseconds> times, Split& split) {
	return (times.size() > splitIndex) ||
		(times.size() == splitIndex && split.hasBestRun() && currentTime > split.getBestRun());
}

void UI::drawSplit(Split& split, bool isActive, milliseconds currentTime, const shared_ptr<ActiveSplitSet> activeSet, int splitIndex) {
	const vector<milliseconds> times = activeSet->getTimes();
	auto settings = Settings::get();

	if (isActive) {
		ImGui::PushStyleColor(ImGuiCol_ChildBg, (ImVec4)settings->activeSplitBgColor); // why this not work ??
		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0,0));
		ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0, 0));
		ImGui::BeginChild("active split", ImVec2(-1, ImGui::GetTextLineHeightWithSpacing()), false);
		ImGui::PopStyleVar(1); // no item spacing in start of child
		//todo other oadding 
	}
	milliseconds spDispTime;
	string nameDisp = split.name.substr(0, 20);
	if (isActive) {
		ImGui::Text(">%-20s", nameDisp.c_str());
	} else {
		ImGui::Text(" %-20s", nameDisp.c_str());
	}
	string diffStr = "";
	ImVec4 diffColor; 
	if (shouldShowDelta(currentTime, splitIndex, times, split)) {
		// split already done uses split time and show diff
		// or current split over time 
		if (times.size() > splitIndex) { // for old splits show their time
			spDispTime = times[splitIndex];
		} else { // otherwise for current split show current time 
			spDispTime = std::chrono::duration_cast<milliseconds>(ourclock::now() - activeSet->getStartTime());
		}
		if (split.hasBestRun()) {
			milliseconds dt = (milliseconds)spDispTime - (milliseconds)split.getBestRun();
			if (isWorse(split, spDispTime)) {
				diffColor = settings->behindLosingColor;
				if (isSegmentImprovement(activeSet, splitIndex)) {
					diffColor = settings->behindGainingColor;
				}
				
				diffStr = "+";
			} else if(isImpovement(split, spDispTime)) {
				diffColor = settings->aheadGainingColor;
				if (isSegmentWorse(activeSet, splitIndex)) {
					diffColor = settings->aheadLosingColor;
				}
				diffStr = "-";
				dt = -dt;
			} else {
				diffColor = settings->equalTimeColor;
				diffStr = " ";
				dt = (milliseconds)0;
			}
			int dms = dt.count() % 1000;
			int dsec = std::chrono::duration_cast<seconds>(dt).count() % 60;
			int dmin = std::chrono::duration_cast<minutes>(dt).count();
			if (dt < seconds(10)) {
				diffStr += string_format("%d.%02d", dsec, (dms / 10));
			} else if (dt < seconds(60)) {
				diffStr += string_format("%d.%01d", dsec, (dms / 100));
			} else {
				diffStr += string_format("%d:%02d", dmin, dsec);
			}
		} else {
			diffStr = "";
		}
		if (isBestSegment(split, activeSet, splitIndex)) {
			diffColor = settings->bestSegmentColor;
		}
	} else {
		// show old times and no diff
		spDispTime = split.getBestRun();
	}
	ImGui::SameLine();
	ImGui::TextColored(diffColor, "%6s", diffStr.c_str());
	int ms_s = spDispTime.count() % 1000;
	int sec_s = std::chrono::duration_cast<seconds>(spDispTime).count() % 60;
	int min_s = std::chrono::duration_cast<minutes>(spDispTime).count();
	ImGui::SameLine();
	if (spDispTime.count() > 0) {
		ImGui::Text(" %02i:%02i.%03i", min_s, sec_s, ms_s);
	} else {
		ImGui::Text(" --:--.---");
	}
	if (isActive) {
		ImGui::EndChild();
		ImGui::PopStyleVar(1);
		ImGui::PopStyleColor(1);
	}
	//ImGui::Separator();
}

void UI::drawActiveSet(milliseconds currentTime, shared_ptr<ActiveSplitSet>& activeSet, float height) {
	ImGui::Text(activeSet->getName().c_str());
	if (ImGui::Button("Split##uid1000") || shouldSplit) {
		Logger::d("[UI] split");
		activeSet->split(currentTime);
	}
	ImGui::SameLine();
	if (ImGui::Button("Skip##uid1001") || shouldSkip) {
		Logger::d("[UI] skip");
		activeSet->skip();
	}
	ImGui::SameLine();
	if (ImGui::Button("Unsplit##uid1003") || shouldUnsplit) {
		Logger::d("[UI] unsplit");
		activeSet->unsplit();
	}
	ImGui::SameLine();
	ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(0.0, 0.6f, 0.6f));
	ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(0.0f, 0.8f, 0.8f));
	ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(0.0f, 0.9f, 0.9f));
	if (ImGui::Button("Reset##uid1004") || shouldReset) {
		Logger::d("[UI] RESET");
		activeSet->reset();
	}
	ImGui::PopStyleColor(3);
	ImGui::Separator();
	const int active = activeSet->getCurrentSplitIndex();
	const float timerSize = Settings::get()->timerSize; 
	const float childHeight = height - ImGui::GetFrameHeightWithSpacing() - ImGui::GetTextLineHeightWithSpacing() * 2 - ImGui_ex::SeperatorHeight() - timerSize  - ImGui::GetStyle().ItemSpacing.y; // one text line, one button line, last splitt line, timer, spacing before timer
	ImGui::BeginChild("Sub_Splits", ImVec2(ImGui::GetWindowContentRegionWidth(), childHeight), false, ImGuiWindowFlags_AlwaysAutoResize);
	int splitIndex = 0;
	for (; splitIndex < activeSet->getSplits().size() - 1; splitIndex++) {
		Split split = activeSet->getSplits()[splitIndex];
		drawSplit(split, splitIndex == active, currentTime, activeSet, splitIndex);
		if (splitIndex == active && splitIndex != lastSplitIndex) {
			ImGui::SetScrollHereY();
			lastSplitIndex = splitIndex;
		}
	}
	ImGui::EndChild();
	// last split always visible
	Split split = activeSet->getSplits()[splitIndex];
	drawSplit(split, splitIndex == active, currentTime, activeSet, splitIndex);

	milliseconds dispTime;
	if (activeSet->getCurrentSplitIndex() == activeSet->getSplits().size()) {
		dispTime = activeSet->getTimes()[activeSet->getCurrentSplitIndex() - 1];
	} else {
		dispTime = std::chrono::duration_cast<milliseconds>(ourclock::now() - activeSet->getStartTime());
	}
	int ms = std::chrono::duration_cast<milliseconds>(dispTime).count() % 1000;
	int sec = std::chrono::duration_cast<seconds>(dispTime).count() % 60;
	int min = std::chrono::duration_cast<minutes>(dispTime).count();
	const float timerWidth = 3.5 * timerSize + 8.f * (int)(timerSize / 4.f);
	const int timerLineThickness = 1 + (int)(timerSize / 20.f); // 1 for 0..19, 2 for 20..39 etc
	ImGui::Indent(270 - timerWidth);
	ImGui_ex::BigTime("bigtimer", timerSize, timerLineThickness, ImU32(0xffffffff), min, sec, ms);
}

void UI::drawMain(int mapId, float* pos, milliseconds currentTime, shared_ptr<ActiveSplitSet> activeSet, vector<shared_ptr<ActiveSplitSet>>& unsafedSplitSets) {
	//ImGui::ShowTestWindow();
	shared_ptr<Settings> settings = Settings::get();
	if (settings->hideSplitsWithoutSet && !activeSet) return;
	ImGui::SetNextWindowSizeConstraints(ImVec2(283, 30), ImVec2(283, FLT_MAX));
	string title = "GW2AutoSplits";
	ImGui::Begin(title.c_str(), &showMain, ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoTitleBar);
	int ms = std::chrono::duration_cast<milliseconds>(currentTime).count() % 1000;
	int sec = std::chrono::duration_cast<seconds>(currentTime).count() % 60;
	int min = std::chrono::duration_cast<minutes>(currentTime).count();
	if(settings->showMapAndPos) {
		ImGui::Text("Mapid: %i   %02i:%02i.%03i", mapId, min, sec, ms);
		ImGui::Text("pos: %.2f %.2f %.2f", pos[0], pos[1], pos[2]);
		ImGui::Separator();
	}
	if (activeSet) {
		float activeSetHeight = ImGui::GetWindowHeight() - ImGui::GetStyle().WindowPadding.y * 2 -  (settings->showMapAndPos ? 2* ImGui::GetTextLineHeightWithSpacing() + ImGui_ex::SeperatorHeight() : 0);
		drawActiveSet(std::chrono::duration_cast<milliseconds>(ourclock::now() - activeSet->getStartTime()), activeSet, activeSetHeight);
	} else {
		lastSplitIndex = -1;
		ImGui::Text("No matching splitset found");
	}
	shouldReset = false;
	shouldSplit = false;
	if (unsafedSplitSets.size() > 0) {
		ImGui::Separator();
		ImGui::Text("Unsafed Splits:");
		for (long i = unsafedSplitSets.size() - 1; i >= 0; i--) {
			ImGui::Text(string_format("%15s", unsafedSplitSets[i]->getName().c_str()).c_str());
			if (ImGui::Button(string_format("save##save_unsafed_set_btn%d",i).c_str())) {
				unsafedSplitSets[i]->saveTimes();
				unsafedSplitSets[i]->writeToFile();
				unsafedSplitSets.erase(unsafedSplitSets.begin() + i);
				Logger::d("[UI] times saved");
			}
			ImGui::SameLine();
			if (ImGui::Button(string_format("discard##discard_unsafed_set_btn%d", i).c_str())) {
				unsafedSplitSets.erase(unsafedSplitSets.begin() + i);
				Logger::d("[UI] times discarded");
			}
		}
	}
	ImGui::End();
}

void UI::draw(int mapId, float* pos, milliseconds time, shared_ptr<ActiveSplitSet> activeSet, vector<shared_ptr<ActiveSplitSet>>& unsafedSplitSets, const MumbleApi& mumbleApi) {
	if(showMain) drawMain(mapId, pos, time, activeSet, unsafedSplitSets);
	if (showSettings) {
		drawSettings();
	} else {
		// force reset input key reading
		currentInputKeyLabel = NULL;
	}
	if(showEditor) splitEditor.drawSplitEditor(activeSet, &showEditor, mumbleApi);
	ImGui::ShowStyleEditor();
}

uintptr_t UI::WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	arc_config cf = readArcConfig();
	uint16_t arc_global_mod1 = cf.mod1Key;
	uint16_t arc_global_mod2 = cf.mod2Key;
	auto const io = &ImGui::GetIO();
	auto sett = Settings::get();
	const int vkey = (int)wParam;
	switch (uMsg) {
	case WM_KEYUP:
	case WM_SYSKEYUP:
		
		io->KeysDown[vkey] = false;
		if (vkey == VK_CONTROL) {
			io->KeyCtrl = false;
		} else if (vkey == VK_MENU) {
			io->KeyAlt = false;
		} else if (vkey == VK_SHIFT) {
			io->KeyShift = false;
		}
		break;
	case WM_KEYDOWN:
	case WM_SYSKEYDOWN:
		if (vkey == VK_CONTROL) {
			io->KeyCtrl = true;
		} else if (vkey == VK_MENU) {
			io->KeyAlt = true;
		} else if (vkey == VK_SHIFT) {
			io->KeyShift = true;
		}
		io->KeysDown[vkey] = true;
		if (currentInputKeyLabel != NULL) {
			if (vkey == VK_ESCAPE) {
				*currentInputKeyTarget = 0;
			} else {
				*currentInputKeyTarget = vkey;
			}
			currentInputKeyLabel = NULL;
			return 0;
		}
		if (vkey == sett->keyReset.keyCode && (!sett->keyReset.requireArcMod1 || io->KeysDown[arc_global_mod1]) && (!sett->keyReset.requireArcMod2 || io->KeysDown[arc_global_mod2])) {
			shouldReset = true;
		}
		if (vkey == sett->keySplit.keyCode && (!sett->keySplit.requireArcMod1 || io->KeysDown[arc_global_mod1]) && (!sett->keySplit.requireArcMod2 || io->KeysDown[arc_global_mod2])) {
			shouldSplit = true;
		}
		if (vkey == sett->keySkip.keyCode && (!sett->keySkip.requireArcMod1 || io->KeysDown[arc_global_mod1]) && (!sett->keySkip.requireArcMod2 || io->KeysDown[arc_global_mod2])) {
			shouldSkip = true;
		}
		if (vkey == sett->keyUnsplit.keyCode && (!sett->keyUnsplit.requireArcMod1 || io->KeysDown[arc_global_mod1]) && (!sett->keyUnsplit.requireArcMod2 || io->KeysDown[arc_global_mod2])) {
			shouldUnsplit = true;
		}
		break;
	case WM_ACTIVATEAPP:
		if (!wParam) {
			io->KeysDown[arc_global_mod1] = false;
			io->KeysDown[arc_global_mod2] = false;
		}
		break;
	default:
		break;
	}
	return uMsg;
}


void writeColorToFloatArray3(ImColor& color, float* float_array) {
	float_array[0] = color.Value.x;
	float_array[1] = color.Value.y;
	float_array[2] = color.Value.z;
}


const map<int, string> keycodes = {
	{VK_LBUTTON, "Mouse 1"},
	{VK_RBUTTON, "Mouse 2"},
	{VK_CANCEL, "CtrBrk"},
	{VK_MBUTTON, "Mouse 3"},
	// VK_XBUTTON1, VK_XBUTTON2
	{VK_BACK, "Back"},
	{VK_TAB, "Tab"},
	{VK_CLEAR, "Clear"},
	{VK_RETURN, "Return"},
	{VK_SHIFT, "Shift"},
	{VK_CONTROL, "Ctrl"},
	{VK_MENU, "Alt"},
	{VK_PAUSE, "Pause"},
	{VK_CAPITAL, "Caps"},
	// varios ime keys
	{VK_ESCAPE, "Escape"},
	// varios ime keys
	{VK_SPACE, "Space"},
	{VK_PRIOR, "Page Up"},
	{VK_NEXT, "Page Down"},
	{VK_END, "End"},
	{VK_HOME, "Home"},
	{VK_LEFT, "Left"},
	{VK_UP, "Up"},
	{VK_RIGHT, "Right"},
	{VK_DOWN, "Down"},
	{VK_SELECT, "Select"},
	{VK_PRINT, "Print"},
	{VK_EXECUTE, "Execute"},
	{VK_SNAPSHOT, "PrtScrn"},
	{VK_INSERT, "Insert"},
	{VK_DELETE, "Delete"},
	{VK_HELP, "Help"},
	// x30 .. x39 -> 0 .. 9
	// x41 .. x5a -> A .. Z
	{VK_LWIN, "Windows"},
	{VK_RWIN, "RWindows"},
	{VK_APPS, "Application"},
	{VK_SLEEP, "Sleep"},
	// x60 .. x69 -> NP0 .. NP9
	{VK_MULTIPLY, "*"},
	{VK_ADD, "+"},
	{VK_SEPARATOR, "|"},
	{VK_SUBTRACT, "-"},
	{VK_DECIMAL, "."},
	{VK_DIVIDE, "/"},
	// x70 .. x87 -> F1 .. F24
	{VK_NUMLOCK, "NumLock"},
	{VK_SCROLL, "ScrollLock"},
	{VK_LSHIFT, "LShift"},
	{VK_RSHIFT, "RShift"},
	{VK_LCONTROL, "LCtrl"},
	{VK_RCONTROL, "RCtrl"},
	{VK_LMENU, "LAlt"},
	{VK_RMENU, "RAlt"},
	// some browser keys, volume keys, media keys
	{VK_OEM_1, "OEM1"},
	{VK_OEM_PLUS, "OEM1"},
	{VK_OEM_COMMA, "OEM+"},
	{VK_OEM_MINUS, "OEM-"},
	{VK_OEM_PERIOD, "OEM."},
	{VK_OEM_2, "OEM2"},
	{VK_OEM_3, "OEM3"},
	// xc1 .. xd7 -> reserved, unasigned 
	{VK_OEM_4, "OEM4"},
	{VK_OEM_5, "OEM5"},
	{VK_OEM_6, "OEM6"},
	{VK_OEM_7, "OEM7"},
	{VK_OEM_8, "OEM8"}
	// some oem, ime and wierd keys
};

const string getKeyName(const int keycode) {
	if (keycode == 0) {
		return "None";
	} else if (('0' <= keycode && keycode <= '9') || 'A' <= keycode && keycode <= 'Z') {
		return string(1, (char)keycode);
	} else if (VK_NUMPAD0 <= keycode && keycode <= VK_NUMPAD9) {
		return string_format("Numpad%d", keycode - VK_NUMPAD0);
	} else if (VK_F1 <= keycode && keycode <= VK_F24) {
		return string_format("F%d", keycode - VK_F1 + 1);
	} else if (keycodes.count(keycode) > 0) {
		return keycodes.at(keycode);
	} else {
		return "<?>";
	}
}

void UI::InputKey(const char* label, Key* key, arc_config& cf) {
	char nameBuff[96];
	ImGui::Text(label);
	ImGui::SameLine();
	ImGui::Dummy(ImVec2(80 - ImGui::CalcTextSize(label).x,0));
	ImGui::SameLine();
	if (currentInputKeyLabel != 0 && strcmp(label, currentInputKeyLabel) == 0) {
		sprintf_s(nameBuff, "> <###bt_ik_%s", label);
		if (ImGui::Button(nameBuff, ImVec2(80, 0))) {
			currentInputKeyLabel = 0;
			currentInputKeyTarget = 0;
		}
	} else {
		sprintf_s(nameBuff, "%s###bt_ik_%s", getKeyName(key->keyCode).c_str(), label);
		if (ImGui::Button(nameBuff, ImVec2(80, 0))) {
			currentInputKeyLabel = label;
			currentInputKeyTarget = &(key->keyCode);
		}
	}
	ImGui::SameLine();
	sprintf_s(nameBuff, "%s##cbam1_%s", getKeyName(cf.mod1Key).c_str(), label);
	ImGui::Checkbox(nameBuff,  &(key->requireArcMod1));
	ImGui::SameLine();
	sprintf_s(nameBuff, "%s##cbam2_%s", getKeyName(cf.mod2Key).c_str(), label);
	ImGui::Checkbox(nameBuff, &(key->requireArcMod2));
}


static bool first_settings_draw = true;

void UI::drawSettings() {
	shared_ptr<Settings> settings = Settings::get();
	ImGui::Begin("GW2AutoSplits Settings", &showSettings, ImGuiWindowFlags_NoCollapse);
	ImGui::Checkbox("Hide GW2AutoSplits when no splitset found", &(settings->hideSplitsWithoutSet));
	ImGui::Checkbox("Write log files", &(settings->writeLogsFiles));
	ImGui::Checkbox("Show while loading and in charater select", &(settings->showInCharSelectAndLoadingScreen));
	ImGui::Checkbox("Show map id and position", &(settings->showMapAndPos));
	ImGui::InputFloat("Timer size", &(settings->timerSize), 1.0, 10.0, "%.1f");
	ImGui::Separator();
	ImGui::Text("Keybindings");
	arc_config cf = readArcConfig();
	InputKey("Split", &(settings->keySplit), cf);
	InputKey("Reset", &(settings->keyReset), cf);
	InputKey("Skip", &(settings->keySkip), cf);
	InputKey("Unsplit", &(settings->keyUnsplit), cf);
	ImGui::Separator();
	static float best_seg[3];
	static float bh_gain[3];
	static float bh_loss[3];
	static float ah_gain[3];
	static float ah_loss[3];
	static float equal_col[3];
	static float active_spbg[3];
	if (first_settings_draw) { // initialize statics
		first_settings_draw = false;
		writeColorToFloatArray3(settings->bestSegmentColor, best_seg);
		writeColorToFloatArray3(settings->behindGainingColor, bh_gain);
		writeColorToFloatArray3(settings->behindLosingColor, bh_loss);
		writeColorToFloatArray3(settings->aheadGainingColor, ah_gain);
		writeColorToFloatArray3(settings->aheadLosingColor, ah_loss);
		writeColorToFloatArray3(settings->equalTimeColor, equal_col);
		writeColorToFloatArray3(settings->activeSplitBgColor, active_spbg);
	}
	ImGui::Text("Colors");
	if (ImGui::ColorEdit3("best segement", best_seg)) {
		settings->bestSegmentColor = ImColor(best_seg[0], best_seg[1], best_seg[2]);
	}
	if (ImGui::ColorEdit3("behind (gaining time)", bh_gain)) {
		settings->behindGainingColor = ImColor(bh_gain[0], bh_gain[1], bh_gain[2]);
	}
	if (ImGui::ColorEdit3("behind (losing time)", bh_loss)) {
		settings->behindLosingColor = ImColor(bh_loss[0], bh_loss[1], bh_loss[2]);
	}
	if (ImGui::ColorEdit3("ahead (gaining time)", ah_gain)) {
		settings->aheadGainingColor = ImColor(ah_gain[0], ah_gain[1], ah_gain[2]);
	}
	if (ImGui::ColorEdit3("ahead (losing time)", ah_loss)) {
		settings->aheadLosingColor = ImColor(ah_loss[0], ah_loss[1], ah_loss[2]);
	}
	if (ImGui::ColorEdit3("equal time", equal_col)) {
		settings->equalTimeColor = ImColor(equal_col[0], equal_col[1], equal_col[2]);
	}
	if (ImGui::ColorEdit3("active split", active_spbg)) {
		settings->activeSplitBgColor = ImColor(active_spbg[0], active_spbg[1], active_spbg[2]);
	}
	ImGui::End();
}
