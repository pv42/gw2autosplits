#include <Windows.h>

#include "timing.h"
#include "string_format.hpp"
#include "logger.h"


ourclock::time_point Timing::bootTime;

void Timing::calculateTimeOffset() {
	DWORD tgNow = timeGetTime();
	ourclock::time_point chronoNow = ourclock::now();
	bootTime = chronoNow - (milliseconds)tgNow;
}

ourclock::time_point Timing::timeGetTimeToChrono(unsigned long tgTime) {
	milliseconds splitEpoch(tgTime); // this is correct
	auto utc_since_epoch = std::chrono::duration_cast<ourclock::duration>(splitEpoch); // this is corrent (unit is 100ns)
	ourclock::time_point splitTime = ourclock::time_point(splitEpoch);
	return bootTime + (milliseconds)tgTime;
}