#pragma once

#include "imgui/imgui.h"
#include <memory>

typedef struct {
	int keyCode;
	bool requireArcMod1;
	bool requireArcMod2;
} Key;

class Settings {
public:
	Settings();
	ImColor bestSegmentColor;
	ImColor aheadGainingColor;
	ImColor aheadLosingColor;
	ImColor behindGainingColor;
	ImColor behindLosingColor;
	ImColor equalTimeColor;
	ImColor activeSplitBgColor;
	bool hideSplitsWithoutSet;
	bool writeLogsFiles;
	bool showMapAndPos;
	bool showInCharSelectAndLoadingScreen;
	Key keySplit;
	Key keyReset;
	Key keySkip;
	Key keyUnsplit;
	float timerSize;
	static const std::shared_ptr<Settings> get();
	void save() const;
private:
	static std::shared_ptr<Settings> settings;
};