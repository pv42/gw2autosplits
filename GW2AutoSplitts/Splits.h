#pragma once
#include <string>
#include <vector>
#include <memory>
#include <mutex>
#include <set>
#include "GW2.h"
#include "arcdps.h"
#include "timing.h"

using std::string;
using std::shared_ptr;
using std::vector;
using std::mutex;
using std::set;


typedef struct arcdps_event {
	cbtevent* event;
	ag* src;
	ag* dst;
	char* skillname;
	uint64_t id;
	uint64_t revision;
} arcdps_event;

class SplitCondition {
public:
	virtual bool isSatisfied(float* pos, arcdps_event* ev) = NULL;
	const string getType() const;
	virtual ~SplitCondition() = NULL {};
	virtual const ourclock::time_point getSplitTime() const;
protected:
	string type;
};

class ArcSplitCondition : public SplitCondition {
public:
	const ourclock::time_point getSplitTime() const;
protected:
	void setSplitTime(uint64_t epoch_ms);
private:
	ourclock::time_point splitTime;
};



class BuffAppliedSplitCondition : public ArcSplitCondition {
public:
	BuffAppliedSplitCondition(const string& target, const SKILL_ID buff);
	bool isSatisfied(float* pos, arcdps_event* ev);
	string target;
	SKILL_ID buff;
};


class CombatSplitCondition : public ArcSplitCondition {
public:
	CombatSplitCondition(const string& target);
	bool isSatisfied(float* pos, arcdps_event* ev);
	string target;
};

class LeftCombatSplitCondition : public ArcSplitCondition {
public:
	LeftCombatSplitCondition(const string& target);
	bool isSatisfied(float* pos, arcdps_event* ev);
	string target;
};

class BoxPositionalSplitCondition : public SplitCondition {
public:
	BoxPositionalSplitCondition(const float lwr_x, const float lwr_y, const float lwr_z, const float upr_x, const float upr_y, const float upr_z);
	BoxPositionalSplitCondition(const float lwr_x, const float lwr_z, const float upr_x, const float upr_z);
	bool isSatisfied(float* pos, arcdps_event* ev);
	float lwr[3], upr[3]; // in order x,y,z
};

class SphericalPositionalSplitCondition : public SplitCondition {
public:
	SphericalPositionalSplitCondition(const float x, const float y, const float z, const float radius);
	bool isSatisfied(float* pos, arcdps_event* ev);
	float mid[3], radius; // in order x,y,z
};

class LogEndSplitCondition : public ArcSplitCondition {
public:
	LogEndSplitCondition();
	LogEndSplitCondition(uint64_t species_id);
	bool isSatisfied(float* pos, arcdps_event* ev);
	uint64_t species_id;
};

class LogStartSplitCondition : public ArcSplitCondition {
public:
	LogStartSplitCondition();
	LogStartSplitCondition(uint64_t species_id);
	bool isSatisfied(float* pos, arcdps_event* ev);
	uint64_t species_id;
};

typedef shared_ptr<SplitCondition> SplitCondition_PTR;
typedef shared_ptr<BuffAppliedSplitCondition> BuffAppliedSplitCondition_PTR;
typedef shared_ptr<CombatSplitCondition> CombatSplitCondition_PRT;
typedef shared_ptr<BoxPositionalSplitCondition>  BoxPositionalSplitCondition_PTR;
typedef shared_ptr<SphericalPositionalSplitCondition> SphericalPositionalSplitCondition_PTR;
typedef shared_ptr<LeftCombatSplitCondition> LeftCombatSplitCondition_PTR;
typedef shared_ptr<LogEndSplitCondition> LogEndSplitCondition_PTR;
typedef shared_ptr<LogStartSplitCondition> LogStartSplitCondition_PTR;

template <class T, class U>
concept Derived = std::is_base_of<U, T>::value;

class Split {
public:
	Split(vector<SplitCondition_PTR> condition, string name);
	Split(const vector<SplitCondition_PTR> condition, vector<SplitCondition_PTR> checkpointConditions, string name, milliseconds bestRun, milliseconds bestSegment, bool checkpoint);
	Split(); // empty init
	Split(Split* other); // copy
	bool isSatisfied(float* pos, arcdps_event* ev, ourclock::time_point* time) const;
	bool isGoToCheckpoint(float* pos, arcdps_event* ev) const;
	const vector<SplitCondition_PTR> getConditions() const;
	const vector<SplitCondition_PTR> getCheckpointConditions() const;
	vector<SplitCondition_PTR>& getConditionsEditable();
	vector<SplitCondition_PTR>& getCheckpointConditionsEditable();
	const bool hasBestRun() const;
	const milliseconds getBestRun() const;
	void updateBestRun(milliseconds time);
	const bool hasBestSegment() const;
	const milliseconds getBestSegment() const;
	void updateBestSegment(milliseconds time);
	void deleteBestTimes();
	string name;
	bool checkpoint;
private:
	vector<SplitCondition_PTR> conditions;
	vector<SplitCondition_PTR> checkpointConditions;
	milliseconds bestRun; // duration from start to this split in best run
	milliseconds bestSegment; // shortest ever time for this split (from prev split)
};

class SplitSet {
public:
	static shared_ptr<SplitSet> loadFromFile(const string& fileName);
	SplitSet(const SplitSet* set);
	SplitSet(string filepath, string name, const int mapId, set<int>, vector<Split>);
	const vector<Split> getSplits() const;
	vector<Split>& getSplitsEditable();
	const string getName() const;
	void setName(const string& newName);
	void writeToFile();
	void deleteBestTimes();
	const string filepath;
	const int mapId;
	set<int> otherMapIds;
	const bool hasMapId(const int mapId) const;
protected:
	vector<Split> splits;
	string name;
};

class ActiveSplitSet : public SplitSet {
public:
	ActiveSplitSet(const shared_ptr<SplitSet> set);
	ActiveSplitSet();
	const Split* getCurrentSplit() const;
	const Split* getPrevSplit() const;
	int getCurrentSplitIndex() const;
	void update(float* pos, arcdps_event* ev, ourclock::time_point time);
	const vector<milliseconds> getTimes() const;
	void saveTimes();
	void reset();
	void split(milliseconds time);
	void skip();
	void unsplit();
	bool isPersonalBest();
	milliseconds getSegmentTime(unsigned int splitIndex) const;
	const ourclock::time_point getStartTime() const;
private:
	vector<milliseconds> times;
	int current_split;
	ourclock::time_point startTime;
	static mutex updateMutex;
};
