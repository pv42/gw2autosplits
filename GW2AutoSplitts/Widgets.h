#pragma once
#include "imgui/imgui.h"

#include <stdint.h>

namespace ImGui_ex {
	bool BigTime(const char* label, float height, int thickness, const ImU32& color, int min, int sec, int ms);
	bool InputUInt64(const char* label, uint64_t* v, int step = 1, int step_fast = 100, ImGuiInputTextFlags flags = 0);
	bool Spinner(const char* label, float radius, float thickness, const ImU32& color);
	float SeperatorHeight();
}

