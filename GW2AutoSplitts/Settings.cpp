#include <fstream>

#include "Settings.h"
#include "json.hpp"
#include "string_format.hpp"
#include <filesystem>
#include "logger.h"

using json = nlohmann::json;
using string = std::string;

std::shared_ptr<Settings> Settings::settings;

constexpr auto KEY_HIDE_SPLITS_WITHOUT_SET = "hide_splits_without_set";
constexpr auto KEY_BEST_SEGMENT_COLOR = "best_segment_color";
constexpr auto KEY_AHEAD_GAINING_COLOR = "ahead_gaining_color";
constexpr auto KEY_AHEAD_LOSING_COLOR = "ahead_losing_color";
constexpr auto KEY_BEHIND_GAINING_COLOR = "behind_gaining_color";
constexpr auto KEY_BEHIND_LOSING_COLOR = "behind_losing_color";
constexpr auto KEY_EQUAL_TIME_COLOR = "equal_time_color";
constexpr auto KEY_ACTIVE_SPLIT_BG_COLOR = "active_split_bg_color";
constexpr auto KEY_WRITE_LOGFILES = "writeLogfiles";
constexpr auto KEY_SHOW_MAP_AND_POS = "showMapAndPos";
constexpr auto KEY_TIMER_SIZE = "timerSize";
constexpr auto KEY_SHOW_IN_CHARS_AND_LOAD = "showInCharSelectAndLoadingScreen";
constexpr auto KEY_KEY_RESET_CODE = "keyCodeReset";
constexpr auto KEY_KEY_RESET_MOD1 = "keyMod1Reset";
constexpr auto KEY_KEY_RESET_MOD2 = "keyMod2Reset";
constexpr auto KEY_KEY_SPLIT_CODE = "keyCodeSplit";
constexpr auto KEY_KEY_SPLIT_MOD1 = "keyMod1Split";
constexpr auto KEY_KEY_SPLIT_MOD2 = "keyMod2Split";
constexpr auto KEY_KEY_SKIP_CODE = "keyCodeSkip";
constexpr auto KEY_KEY_SKIP_MOD1 = "keyMod1Skip";
constexpr auto KEY_KEY_SKIP_MOD2 = "keyMod2Skip";
constexpr auto KEY_KEY_UNSPLIT_CODE = "keyCodeUnsplit";
constexpr auto KEY_KEY_UNSPLIT_MOD1 = "keyMod1Unsplit";
constexpr auto KEY_KEY_UNSPLIT_MOD2 = "keyMod2Unsplit";

void readColor(const json& j, const string& key, ImColor& target) {
	if (!j.contains(key)) return;
	string clrStr = j[key];
	int r, g, b, a;
	if (sscanf_s(clrStr.c_str(), "#%02x%02x%02x%02x", &r, &g, &b, &a) < 4) return;
	target = ImColor(r, g, b, a);
}

Settings::Settings() :
	hideSplitsWithoutSet(false),
	bestSegmentColor(216, 175, 31),
	aheadGainingColor(0, 204, 54),
	aheadLosingColor(82, 204, 115),
	behindGainingColor(204, 92, 82),
	behindLosingColor(204, 18, 0),
	equalTimeColor(255, 255, 255),
	activeSplitBgColor(27, 110, 145),
	writeLogsFiles(true),
	showMapAndPos(false),
	showInCharSelectAndLoadingScreen(false),
	timerSize(30)
{
	keyReset.keyCode = 0;
	keyReset.requireArcMod1 = true;
	keyReset.requireArcMod2 = true;
	keySplit.keyCode = 0;
	keySplit.requireArcMod1 = true;
	keySplit.requireArcMod2 = true;
	std::filesystem::path path = std::filesystem::current_path() / "addons" / "gw2AutoSplits" / "settings.json";
	if (std::filesystem::exists(path)) {
		json j;
		std::ifstream file(path);
		file >> j;
		file.close();
		if (j.contains(KEY_HIDE_SPLITS_WITHOUT_SET)) hideSplitsWithoutSet = j[KEY_HIDE_SPLITS_WITHOUT_SET];
		if (j.contains(KEY_WRITE_LOGFILES)) writeLogsFiles = j[KEY_WRITE_LOGFILES];
		if (j.contains(KEY_SHOW_MAP_AND_POS)) showMapAndPos = j[KEY_SHOW_MAP_AND_POS];
		if (j.contains(KEY_TIMER_SIZE)) timerSize = j[KEY_TIMER_SIZE];
		if (j.contains(KEY_SHOW_IN_CHARS_AND_LOAD)) showInCharSelectAndLoadingScreen = j[KEY_SHOW_IN_CHARS_AND_LOAD];
		if (j.contains(KEY_KEY_RESET_CODE)) keyReset.keyCode = j[KEY_KEY_RESET_CODE];
		if (j.contains(KEY_KEY_RESET_MOD1)) keyReset.requireArcMod1 = j[KEY_KEY_RESET_MOD1];
		if (j.contains(KEY_KEY_RESET_MOD2)) keyReset.requireArcMod2 = j[KEY_KEY_RESET_MOD2];
		if (j.contains(KEY_KEY_SPLIT_CODE)) keySplit.keyCode = j[KEY_KEY_SPLIT_CODE];
		if (j.contains(KEY_KEY_SPLIT_MOD1)) keySplit.requireArcMod1 = j[KEY_KEY_SPLIT_MOD1];
		if (j.contains(KEY_KEY_SPLIT_MOD2)) keySplit.requireArcMod2 = j[KEY_KEY_SPLIT_MOD2];
		if (j.contains(KEY_KEY_SKIP_CODE)) keySkip.keyCode = j[KEY_KEY_SKIP_CODE];
		if (j.contains(KEY_KEY_SKIP_MOD1)) keySkip.requireArcMod1 = j[KEY_KEY_SKIP_MOD1];
		if (j.contains(KEY_KEY_SKIP_MOD2)) keySkip.requireArcMod2 = j[KEY_KEY_SKIP_MOD2];
		if (j.contains(KEY_KEY_UNSPLIT_CODE)) keyUnsplit.keyCode = j[KEY_KEY_UNSPLIT_CODE];
		if (j.contains(KEY_KEY_UNSPLIT_MOD1)) keyUnsplit.requireArcMod1 = j[KEY_KEY_UNSPLIT_MOD1];
		if (j.contains(KEY_KEY_UNSPLIT_MOD2)) keyUnsplit.requireArcMod2 = j[KEY_KEY_UNSPLIT_MOD2];
		readColor(j, KEY_BEST_SEGMENT_COLOR, bestSegmentColor);
		readColor(j, KEY_AHEAD_GAINING_COLOR, aheadGainingColor);
		readColor(j, KEY_AHEAD_LOSING_COLOR, aheadLosingColor);
		readColor(j, KEY_BEHIND_GAINING_COLOR, behindGainingColor);
		readColor(j, KEY_BEHIND_LOSING_COLOR, behindLosingColor);
		readColor(j, KEY_EQUAL_TIME_COLOR, equalTimeColor);
		readColor(j, KEY_ACTIVE_SPLIT_BG_COLOR, activeSplitBgColor);
	} else {
		Logger::i(string_format("[Settings] Settings file %s not found , using default values", path.c_str()));
	}
}

static inline float Saturate(float f) { return (f < 0.0f) ? 0.0f : (f > 1.0f) ? 1.0f : f; } // limits float to [0..1]

void setColor(json& j, const string& key, const ImColor& color) {
	j[key] = json();
	int r = ((int)(Saturate(color.Value.x) * 255.0f + 0.5f));
	int g = ((int)(Saturate(color.Value.y) * 255.0f + 0.5f));
	int b = ((int)(Saturate(color.Value.z) * 255.0f + 0.5f));
	int a = ((int)(Saturate(color.Value.w) * 255.0f + 0.5f));
	j[key] = string_format("#%02x%02x%02x%02x", r, g, b, a);
}

void Settings::save() const {
	json main;
	main[KEY_HIDE_SPLITS_WITHOUT_SET] = hideSplitsWithoutSet;
	main[KEY_SHOW_MAP_AND_POS] = showMapAndPos;
	main[KEY_WRITE_LOGFILES] = writeLogsFiles;
	main[KEY_TIMER_SIZE] = timerSize;
	main[KEY_SHOW_IN_CHARS_AND_LOAD] = showInCharSelectAndLoadingScreen;
	main[KEY_KEY_RESET_CODE] = keyReset.keyCode;
	main[KEY_KEY_RESET_MOD1] = keyReset.requireArcMod1;
	main[KEY_KEY_RESET_MOD2] = keyReset.requireArcMod2;
	main[KEY_KEY_SPLIT_CODE] = keySplit.keyCode;
	main[KEY_KEY_SPLIT_MOD1] = keySplit.requireArcMod1;
	main[KEY_KEY_SPLIT_MOD2] = keySplit.requireArcMod2;
	main[KEY_KEY_SKIP_CODE] = keySkip.keyCode;
	main[KEY_KEY_SKIP_MOD1] = keySkip.requireArcMod1;
	main[KEY_KEY_SKIP_MOD2] = keySkip.requireArcMod2;
	main[KEY_KEY_UNSPLIT_CODE] = keyUnsplit.keyCode;
	main[KEY_KEY_UNSPLIT_MOD1] = keyUnsplit.requireArcMod1;
	main[KEY_KEY_UNSPLIT_MOD2] = keyUnsplit.requireArcMod2;
	setColor(main, KEY_BEST_SEGMENT_COLOR, bestSegmentColor);
	setColor(main, KEY_AHEAD_GAINING_COLOR, aheadGainingColor);
	setColor(main, KEY_AHEAD_LOSING_COLOR, aheadLosingColor);
	setColor(main, KEY_BEHIND_GAINING_COLOR, behindGainingColor);
	setColor(main, KEY_BEHIND_LOSING_COLOR, behindLosingColor);
	setColor(main, KEY_EQUAL_TIME_COLOR, equalTimeColor);
	setColor(main, KEY_ACTIVE_SPLIT_BG_COLOR, activeSplitBgColor);
	string filepath;
	std::filesystem::path path = std::filesystem::current_path() / "addons" / "gw2AutoSplits" / "settings.json";
	std::ofstream file(path);
	file << main.dump(2);
	file.close(); 
}

const std::shared_ptr<Settings> Settings::get() {
	if (settings == NULL) {
		settings = std::make_shared<Settings>();
	}
	return settings;
}
