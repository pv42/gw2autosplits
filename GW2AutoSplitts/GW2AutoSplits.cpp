// GW2AutoSplits.cpp
//

#include <stdint.h>
#include <stdio.h>
#include <vector>
#include <string>
#include <mutex>
#include <filesystem>

#include "GW2AutoSplits.h"
#include "MumbleApi.h"
#include "imgui/imgui.h"
#include "arcdps.h"
#include "string_format.hpp"
#include "GW2.h"
#include "logger.h"
#include "Settings.h"



std::unique_ptr<GW2AutoSplits> GW2AutoSplits::gw2autoSplits;
bool GW2AutoSplits::crashed;

path getUserSplitBasePath() {
	char* userpath_c;
	int ret = _dupenv_s(&userpath_c, NULL, "USERPROFILE"); // i dont like this
	if (ret != 0 || userpath_c == NULL) {
		Logger::e("could not fetch user home skipping log file writting");
		return path();
	}
	string userpath = userpath_c;
	free(userpath_c);
	// ensure path exists
	if (!std::filesystem::exists(path(userpath) / "Documents" / "Guild Wars 2")) {
		Logger::i("[MAIN] Creating direcory Documents/Guild Wars 2");
		std::filesystem::create_directory(path(userpath) / "Documents" / "Guild Wars 2");
	}
	if (!std::filesystem::exists(path(userpath) / "Documents" / "Guild Wars 2" / "addons")) {
		Logger::i("[MAIN] Creating direcory Documents/Guild Wars 2/addons");
		std::filesystem::create_directory(path(userpath) / "Documents" / "Guild Wars 2" / "addons");
	}
	if (!std::filesystem::exists(path(userpath) / "Documents" / "Guild Wars 2" / "addons" / "gw2autosplits")) {
		Logger::i("[MAIN] Creating direcory Documents/Guild Wars 2/addons/gw2autosplits");
		std::filesystem::create_directory(path(userpath) / "Documents" / "Guild Wars 2" / "addons" / "gw2autosplits");
	}
	return path(userpath) / "Documents" / "Guild Wars 2" / "addons" / "gw2autosplits";
}

const path GW2AutoSplits::getSplitsPath() {
	return getUserSplitBasePath() / "splitsets";
}

bool GW2AutoSplits::s_is_init() {
	return gw2autoSplits != NULL;
}

intptr_t GW2AutoSplits::s_wnd_proc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	if (!GW2AutoSplits::s_is_init()) {
		return uMsg;
	}
	return GW2AutoSplits::gw2autoSplits->WindowProc(hWnd, uMsg, wParam, lParam);
}

uintptr_t GW2AutoSplits::s_combat(cbtevent* ev, ag* src, ag* dst, char* skillname, uint64_t id, uint64_t revision) {
	if (crashed) return 0;
	try {
		if (!GW2AutoSplits::s_is_init()) {
			return 0;
		}
		return GW2AutoSplits::gw2autoSplits->CombatEvent(ev, src, dst, skillname, id, revision);
	} catch (std::exception& e) {
		Logger::e("[MAIN] ERROR: uncaught exception in combat event:");
		Logger::e(const_cast<char*>(e.what()));
		crashed = true;
	}
}

uintptr_t GW2AutoSplits::s_combat_local(cbtevent* ev, ag* src, ag* dst, char* skillname, uint64_t id, uint64_t revision) {
	if (!GW2AutoSplits::s_is_init()) {
		return 0;
	}
	//return GW2AutoSplits::gw2autoSplits->CombatEventLocal(ev, src, dst, skillname, id, revision);
	return 0;
}

arcdps_exports* GW2AutoSplits::s_init() {
	crashed = false;
	return GW2AutoSplits::gw2autoSplits->Init(); 
}

uintptr_t GW2AutoSplits::s_release() {
	return GW2AutoSplits::gw2autoSplits->Release();
}

uintptr_t GW2AutoSplits::s_imgui(uint32_t not_charsel_or_loading) {
	if (!GW2AutoSplits::s_is_init()) {
		Logger::w(string_format("[MAIN] imgui not init: %d", gw2autoSplits.get()));
		return 0;
	}
	return GW2AutoSplits::gw2autoSplits->ImGui(not_charsel_or_loading);
}

void GW2AutoSplits::s_uiOptions() {
	if (crashed) return;
	try {
		if (!GW2AutoSplits::s_is_init()) {
			return;
		}
		GW2AutoSplits::gw2autoSplits->ui.options();
	} catch (std::exception& e) {
		Logger::e("[MAIN] ERROR: ex in uiOptions::");
		Logger::e(const_cast<char*>(e.what()));
		crashed = true;
	}
}

GW2AutoSplits::GW2AutoSplits(char* arcversionstr) {
	this->arcVersion = arcversionstr;
	activeSplitSet = NULL;
}

uintptr_t GW2AutoSplits::ImGui(uint32_t not_charsel_or_loading) {
	if(!crashed) {
		try {
			if (!not_charsel_or_loading && !Settings::get()->showInCharSelectAndLoadingScreen) return 0;
			ourclock::time_point now = ourclock::now();
		
			milliseconds time = std::chrono::duration_cast<milliseconds>(now -  start);
			float* pos = mumbleApi.getPosition();
			ui.draw(mumbleApi.getMapId(), pos, time, activeSplitSet, unsafedSplitSets, mumbleApi);
			if (activeSplitSet != NULL) {
				activeSplitSet->update(pos, NULL, now);
			}
		} catch (std::exception& e) {
			Logger::e("[MAIN] ERROR: ex in ui draw::");
			Logger::e(const_cast<char*>(e.what()));
			crashed = true;
		}
	}
	return 0;
}
void GW2AutoSplits::loadAllSplitFiles() {
	const path path = GW2AutoSplits::getSplitsPath();
	Logger::i(string_format("loading splits from %s", path.string().c_str()));
	for (const auto& entry : std::filesystem::directory_iterator(path)) {
		if (!entry.is_directory() && entry.path().extension() == ".json") {
			loadSplitSet(entry.path().string());
		} else {
			Logger::d("[MAIN] Skipping path:" + entry.path().string());
		}
	}
}

arcdps_exports* GW2AutoSplits::Init() {
	// init time offset
	Timing::calculateTimeOffset();
	mumbleApi.initialize();
	loadAllSplitFiles();
	arc_exports.sig = 0x69420;
	arc_exports.imguivers = 18000;
	arc_exports.size = sizeof(arcdps_exports);
	arc_exports.out_name = "GW2AutoSplits";
	arc_exports.out_build = __DATE__;
	arc_exports.wnd_nofilter = GW2AutoSplits::s_wnd_proc;
	arc_exports.wnd_filter = NULL;
	arc_exports.combat = GW2AutoSplits::s_combat;
	arc_exports.combat_local = GW2AutoSplits::s_combat_local;
	arc_exports.imgui = GW2AutoSplits::s_imgui;
	arc_exports.options_windows = NULL;
	arc_exports.options_end = GW2AutoSplits::s_uiOptions;
	arc_log("loaded GW2AutoSplits\n");
	if (!gw2autoSplits) {
		Logger::e("[MAIN] init did not work :/");
	} else if (gw2autoSplits.get() != this) {
		Logger::w("[MAIN] init value is different");
	} else {
		Logger::d(string_format("[MAIN] loaded gw2autosplits obj at %010x", (void*)gw2autoSplits.get()));
	}
	return &arc_exports;
}


uintptr_t GW2AutoSplits::Release() {
	mumbleApi.close();
	// save settings
	Settings::get()->save();

	WriteArcLogFile("unloaded GW2AutoSplits");
	Logger::free();
	return 0;
}

uintptr_t GW2AutoSplits::WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	return ui.WindowProc(hWnd, uMsg, wParam, lParam);
}

bool checkForName(ag* src, ag* dst, const string& target_name) {
	return (dst && dst->name && !target_name.compare(dst->name)) || (src && src->name && !target_name.compare(src->name));
}

void GW2AutoSplits::logSplit(const string& name, ourclock::time_point time) {
	std::chrono::duration<float> secs = time - start;
	arc_log(string_format("Split %s %f", name.c_str(), secs.count()));
}

string sanatizeFolderName(const string& in) {
	char illegalChars[] = "[]/\\:*?\"<>|";
	string out = in;
	for (int i = 0; i < in.size(); i++) {
		for (int j = 0; j < sizeof(illegalChars); j++) {
			if (in.at(i) == illegalChars[j]) {
				out = out.replace(i, 1, 1, '-');
			}
		}
	}
	return out;
}

void writeSplitLog(const shared_ptr<const ActiveSplitSet> set) {
	path logsPath = getUserSplitBasePath() / "logs";
	if (!std::filesystem::exists(logsPath )) {
		// todo softcode printed path
		Logger::i("[MAIN] Creating direcory Documents/Guild Wars 2/addons/gw2autosplits/logs");
		std::filesystem::create_directory(logsPath);
	}
	string folderName = sanatizeFolderName(set->getName());
	if (!std::filesystem::exists(logsPath / folderName)) {
		// todo softcode printed path
		Logger::i(string_format("[MAIN] Creating direcory Documents/Guild Wars 2/addons/gw2autosplits/logs/%s", folderName.c_str()));
		std::filesystem::create_directory(logsPath / folderName);
	}
	path p;
	time_t t = std::time(nullptr);
	tm tm;
	localtime_s(&tm, &t);
	std::ostringstream oss;
	oss << std::put_time(&tm, "%d-%m-%Y-%H-%M-%S");
	string timestr = oss.str();
	string filename = string_format("%s.txt", timestr.c_str());
	p = logsPath / folderName / filename;
	Logger::d(string_format("writing logfile to %s", p.string().c_str()));
	ofstream file(p);
	file << set->getName();
	file << ";";
	file << timestr;
	file << std::endl;
	file << "split number;split name; split time in ms; best run time in ms; best segment time in ms";
	file << std::endl;
	int i = 0;
	for (Split split : set->getSplits()) {
		milliseconds time = (milliseconds)0;
		if (i < set->getTimes().size()) {
			time = set->getTimes()[i];
		}
		Split split = set->getSplits()[i];
		file << string_format("%d;%s;%d;%d;%d",
			i+1,
			split.name.c_str(),
			time.count(),
			split.getBestRun().count(),
			split.getBestSegment().count());
		i++;
		file << std::endl;
	}
	file.close();
}

// todo remove
void writebackActiveSet(const shared_ptr<ActiveSplitSet>& active, vector<shared_ptr<SplitSet>> sets) {
	for (int i = 0; i < sets.size(); i++) {
		if (sets[i]->mapId == active->mapId) {
			sets[i] = std::make_shared<SplitSet>(active.get());
			break;
		}
	}
}

// returns true when set was changed (not same)
bool GW2AutoSplits::selectSplitSet(const int mapId) {
	bool useNewSplitSet = activeSplitSet == NULL || !activeSplitSet->hasMapId(mapId);
	for (int i = 0; i < splitSets.size(); i++) {
		shared_ptr<SplitSet> set = splitSets[i];
		if (set->mapId == mapId) {
			if (useNewSplitSet) {
				if (activeSplitSet != NULL) {
					// writeback times
					//writebackActiveSet(activeSplitSet, splitSets);
					// push in save queue if pb
					if (activeSplitSet->isPersonalBest()) {
						Logger::d(string_format("[MAIN] push back unsafed for new one"));
						unsafedSplitSets.push_back(activeSplitSet);
					}
					if (Settings::get()->writeLogsFiles) writeSplitLog(activeSplitSet);
				}
				activeSplitSet = std::make_shared<ActiveSplitSet>(set);
				Logger::i(string_format("[MAIN] using split set %s for map %d", set->getName().c_str(), set->mapId));
				return true;
			} else {
				break;
			}
		}
	}
	
	if(useNewSplitSet) {
		if (activeSplitSet != NULL) {
			// writeback times
			//writebackActiveSet(activeSplitSet, splitSets);
			// push in save queue if pb
			if (activeSplitSet->isPersonalBest()) {
				Logger::d(string_format("[MAIN] push back unsafed without new (%d times)", activeSplitSet->getTimes().size()));
				unsafedSplitSets.push_back(activeSplitSet);
			}
			if (Settings::get()->writeLogsFiles) writeSplitLog(activeSplitSet);
		}
		Logger::d(string_format("[MAIN] Could not find splitset for map %d", mapId));
		activeSplitSet = NULL;
		return true;
	}
	return false;
}

void GW2AutoSplits::loadSplitSet(const string& filename) {
	try {
		const string filepath(filename);
		std::shared_ptr<SplitSet> set = SplitSet::loadFromFile(filepath);
		if (set == NULL) {
			Logger::w(string_format("set load for %s retured NULL", filename.c_str()));
			return;
		}
		splitSets.push_back(set);
	} catch (std::exception e) {
		Logger::e(string_format("Failed to load file %s: %s", filename.c_str(), e.what()));
	}
}

uintptr_t GW2AutoSplits::CombatEvent(cbtevent* event, ag* src, ag* dst, char* skillname, uint64_t id, uint64_t revision) {
	auto now = ourclock::now();
	int mapId = mumbleApi.getMapId();
	if (!event && dst && dst->self) { // map load: check for maybe new set // todo: do map check with mumble
		Logger::i("[CBEV] RESET");
		Logger::i(string_format("[CBEV] map id is %d", mapId));
		if (selectSplitSet(mapId)) {
			start = now;
		}
		char buff[64];
		sprintf_s(buff, "mod1 is %d", readArcConfig().mod1Key);
		WriteArcLogFile(buff);
		sprintf_s(buff, "mod2 is %d", readArcConfig().mod2Key);
		WriteArcLogFile(buff);

	} else {
		if (activeSplitSet != NULL) {
			arcdps_event ev;
			ev.event = event;
			ev.src = src;
			ev.dst = dst;
			ev.skillname = skillname;
			ev.id = id;
			ev.revision = revision;
			activeSplitSet->update(mumbleApi.getPosition(), &ev, now);
		}
	}
	if (event && event->is_statechange == CBTS_LOGSTART) {
		Logger::d(string_format("[MAIN] Logstart id:%d -> %s", event->src_agent, SpeciesIdStorage::getNameById(event->src_agent).c_str()));
	}
	if (event && event->is_statechange == CBTS_LOGEND) {
		Logger::d(string_format("[MAIN] Logend id:%d -> %s", event->src_agent, SpeciesIdStorage::getNameById(event->src_agent).c_str()));
	}
	if (event && event->is_statechange == CBTS_REWARD) {
		Logger::d(string_format("[MAIN] Reward da(id):%d va(type):%d", event->dst_agent, event->value));
	}
	try {
		if (!SpeciesIdStorage::isLoading() && !SpeciesIdStorage::isLoaded()) {
			SpeciesIdStorage::init();
		}
	} catch (std::exception e) {
		Logger::e("ex in sids init");
		Logger::e(const_cast<char*>(e.what()));
	}
	return 0;
}

uintptr_t GW2AutoSplits::CombatEventLocal(cbtevent* event, ag* src, ag* dst, char* skillname, uint64_t id, uint64_t revision) {
	if (crashed) return 0;
	try {
		auto now = ourclock::now();
		int mapId = mumbleApi.getMapId();
		if (mapId == MAP_SUNQUA_PEAK || mapId == MAP_SHATTERED_OBSERVATORY) {
			eventLog(event, src, dst, skillname, id, revision);
		}
		return 0;
	} catch (std::exception& e) {
		Logger::e("[CBEV] ERROR: uncaught exception in local cmb ev");
		Logger::e(const_cast<char*>(e.what()));
		crashed = true;
	}
	return 0;
}

bool is_spam(SKILL_ID skillid) {
	return is_buff(skillid) || is_condition(skillid) || is_guard(skillid)|| is_fractal(skillid);
}

void GW2AutoSplits::eventLog(cbtevent* event, ag* src, ag* dst, char* skillname, uint64_t id, uint64_t revision) {
	/* ev is null. dst will only be valid on tracking add. skillname will also be null */
	string message = "";
	if (!event) {
		/* notify tracking change */
		if (!src->elite) {
			/* add */
			if (src->prof) {
				message += "==== cbtnotify ====\n";
				message += string_format("agent added: %s:%s (%0llx), instid: %u, prof: %u, elite: %u, self: %u, team: %u, subgroup: %u\n", src->name, dst->name, src->id, dst->id, dst->prof, dst->elite, dst->self, src->team, dst->team);
			}
			/* remove */
			else {
				message += "==== cbtnotify ====\n";
				message += string_format("agent removed: %s (%0llx)\n", src->name, src->id);
			}
		}
		/* notify target change */
		else if (src->elite == 1) {
			message += ("==== cbtnotify ====\n");
			message += string_format("new target: id:%0llx elite:%0llx name:%s prof:%0llx\n", src->id, src->elite, src->name, src->prof);
			arc_log("START");
		}
	} else {
		/* default names */
		if (!src->name || !strlen(src->name)) src->name = (char*)"(area)";
		if (!dst->name || !strlen(dst->name)) dst->name = (char*)"(area)";
		/* common */
		message += string_format("==== cbtevent %u at %llu ====\n", 0, event->time);
		message += string_format("source agent: %s (%0llx:%u, %lx:%lx), master: %u\n", src->name, event->src_agent, event->src_instid, src->prof, src->elite, event->src_master_instid);
		if (event->dst_agent) message += string_format("target agent: %s (%0llx:%u, %lx:%lx)\n", dst->name, event->dst_agent, event->dst_instid, dst->prof, dst->elite);
		else message += string_format("target agent: n/a\n");
		/* statechange */
		if (event->is_statechange) {
			message += string_format("is_statechange: %u\n", event->is_statechange);
		}
		/* activation */
		else if (event->is_activation) {
			if (is_spam((SKILL_ID)event->skillid)) return; // reduce the spam
			message += string_format("is_activation: %u\n", event->is_activation);
			message += string_format("skill: %s:%u\n", skillname, event->skillid);
			message += string_format("ms_expected: %d\n", event->value);
		}
		/* buff remove */
		else if (event->is_buffremove) {
			if (is_spam((SKILL_ID)event->skillid)) return; // reduce the spam
			message += string_format("is_buffremove: %u\n", event->is_buffremove);
			message += string_format("skill: %s:%u\n", skillname, event->skillid);
			message += string_format("ms_duration: %d\n", event->value);
			message += string_format("ms_intensity: %d\n", event->buff_dmg);
		}
		/* buff */
		else if (event->buff) {
			if (is_spam((SKILL_ID)event->skillid)) return; // reduce the spam
			/* damage */
			if (event->buff_dmg) {
				message += string_format("is_buff: %u\n", event->buff);
				message += string_format("skill: %s:%u\n", skillname, event->skillid);
				message += string_format("dmg: %d\n", event->buff_dmg);
				message += string_format("is_shields: %u\n", event->is_shields);
			}
			/* application */
			else {
				if (is_spam((SKILL_ID)event->skillid)) return; // reduce the spam
				message += string_format("is_buff: %u\n", event->buff);
				message += string_format("skill: %s:%u\n", skillname, event->skillid);
				message += string_format("raw ms: %d\n", event->value);
				message += string_format("overstack ms: %u\n", event->overstack_value);
			}
		}

		/* physical */
		else {
			if (is_spam((SKILL_ID)event->skillid)) return; // reduce the spam
			message += string_format("is_buff: %u\n", event->buff);
			message += string_format("skill: %s:%u\n", skillname, event->skillid);
			message += string_format("dmg: %d\n", event->value);
			message += string_format("is_moving: %u\n", event->is_moving);
			message += string_format("is_ninety: %u\n", event->is_ninety);
			message += string_format("is_flanking: %u\n", event->is_flanking);
			message += string_format("is_shields: %u\n", event->is_shields);
		}

		/* common */
		message += string_format("iff: %u\n", event->iff);
		message += string_format("result: %u\n", event->result);
	}
	message += string_format("id:%u\n", id);
	arc_log(message);
}

void GW2AutoSplits::arc_log(string str) {
	str = "GW2AS:" + str + '\n';
	WriteArcLogFile(const_cast<char*>(str.c_str()));
}


