#pragma once
#include <stdint.h>
#include <vector>
#include <string>
#include <utility> // pair

using std::vector;
using std::string;
using std::pair;


class StringSearchStruct {
public:
	StringSearchStruct();
	void addElement(const string str, const uint64_t i);
	vector<pair<uint64_t, string>> search(const string searchStr, int limit) const;
private:
	vector<vector<vector<pair<uint64_t, string>>>> data;
};

