#pragma once

#include <chrono>

using ourclock = std::chrono::steady_clock;
using std::chrono::milliseconds;
using std::chrono::milliseconds;
using std::chrono::seconds;
using std::chrono::minutes;

class Timing {
public:
	static ourclock::time_point timeGetTimeToChrono(unsigned long);
	static void calculateTimeOffset();
private:
	static ourclock::time_point bootTime;
};