#include "Widgets.h"
#include "imgui/imgui_internal.h"
#include <stdint.h>

/**
 * seven segment dispaly
 *  ---    <- 0    0-23-56789
 * |   |   <- 1,2  0---456-89, 01234--789 
 *  ---    <- 3    --23456-89
 * |   |   <- 4,5  0-2---6-8-, 01-3456789
 *  ---    <- 6    0-23-56-89
 */

#define T true
#define F false

constexpr bool SEGMENTS[10][7] = {
	{T, T, T, F, T, T, T}, //0
	{F, F, T, F, F, T, F}, //1
	{T, F, T, T, T, F, T}, //2
	{T, F, T, T, F, T, T}, //3
	{F, T, T, T, F, T, F}, //4
	{T, T, F, T, F, T, T}, //5
	{T, T, F, T, T, T, T}, //6
	{T, F, T, F, F, T, F}, //7
	{T, T, T, T, T, T, T}, //8
	{T, T, T, T, F, T, T}, //9
};

#undef T
#undef F


namespace ImGui_ex {
	void hLine(ImGuiWindow* window, float x1, float x2, float y, float thickness, const ImU32& color) {
		window->DrawList->AddLine(ImVec2(x1, y), ImVec2(x2, y), color, thickness);

	}

	void vLine(ImGuiWindow* window, float x, float y1, float y2, float thickness, const ImU32& color) {
		window->DrawList->AddLine(ImVec2(x, y1), ImVec2(x, y2), color, thickness);
	}

	void digit(ImGuiWindow* window, ImVec2& pos, float digitWidth, float height, float thickness, const ImU32& color, int digit) {
		digit %= 10;
		// 2 x and 3 y coords of intrest
		float x_left = pos.x;
		float x_right = pos.x + digitWidth;
		float y_upr = pos.y;
		float y_mid = pos.y + height / 2.0;
		float y_lwr = pos.y + height;
		// 6 dots of intrest (corners)
		ImVec2 A(x_left, y_upr);
		ImVec2 B(x_right, y_upr);
		ImVec2 C(x_left, y_mid);
		ImVec2 D(x_right, y_mid);
		ImVec2 E(x_left, y_lwr);
		ImVec2 F(x_right, y_lwr);
		const bool* segs = SEGMENTS[digit % 10];
		if (segs[0]) {
			hLine(window, x_left, x_right, y_upr, thickness, color);
		}
		if (segs[1]) {
			vLine(window, x_left, y_upr, y_mid, thickness, color);
		}
		if (segs[2]) {
			vLine(window, x_right, y_upr, y_mid, thickness, color);
		}
		if (segs[3]) {

			hLine(window, x_left, x_right, y_mid, thickness, color);
		}
		if (segs[4]) {
			vLine(window, x_left, y_mid, y_lwr, thickness, color);
		}
		if (segs[5]) {
			vLine(window, x_right, y_mid, y_lwr, thickness, color);
		}
		if (segs[6]) {
			hLine(window, x_left, x_right, y_lwr, thickness, color);
		}
	}

	bool BigTime(const char* label, float height, int thickness, const ImU32& color, int min, int sec, int ms) {
		ImGuiWindow* window = ImGui::GetCurrentWindow();
		if (window->SkipItems)
			return false;
		ImGuiContext& g = *GImGui;
		const ImGuiStyle& style = g.Style;
		const ImGuiID id = window->GetID(label);
		ImVec2 pos = window->DC.CursorPos;
		const float digitWidth = height / 2;
		const float digitSpacing = (int)(digitWidth / 2);
		// width = 3.5 * height + 8 * int(height/4)
		ImVec2 size(digitWidth * 7 + 8 * digitSpacing, height); // 7 digigts, 6 spacings
		const ImRect bb(pos, ImVec2(pos.x + size.x, pos.y + size.y));
		ImGui::ItemSize(bb, style.FramePadding.y);
		if (!ImGui::ItemAdd(bb, id))
			return false;
		window->DrawList->PathClear();
		if (thickness % 2 == 0) { // force full pixel aligned lines if possible 
			pos.x += 0.5;
			pos.y += 0.5;
		}
		if (min >= 10) {
			digit(window, pos, digitWidth, height, thickness, color, min / 10);
		}
		pos.x += digitWidth + digitSpacing;
		digit(window, pos, digitWidth, height, thickness, color, min % 10);
		pos.x += digitWidth + digitSpacing;
		window->DrawList->AddRectFilled(ImVec2(pos.x - 1, pos.y + .35 * height - 1), ImVec2(pos.x + 1, pos.y + .35 * height + 1), color);
		window->DrawList->AddRectFilled(ImVec2(pos.x - 1, pos.y + .65 * height - 1), ImVec2(pos.x + 1, pos.y + .65 * height + 1), color);
		pos.x += digitSpacing;
		digit(window, pos, digitWidth, height, thickness, color, sec / 10);
		pos.x += digitWidth + digitSpacing;
		digit(window, pos, digitWidth, height, thickness, color, sec % 10);
		pos.x += digitWidth + digitSpacing;
		window->DrawList->AddRectFilled(ImVec2(pos.x - 1, pos.y + .9 * height - 1), ImVec2(pos.x + 1, pos.y + .9 * height + 1), color);
		pos.x += digitSpacing;
		digit(window, pos, digitWidth, height, thickness, color, ms / 100);
		pos.x += digitWidth + digitSpacing;
		digit(window, pos, digitWidth, height, thickness, color, (ms / 10) % 10);
		pos.x += digitWidth + digitSpacing;
		digit(window, pos, digitWidth, height, thickness, color, ms % 10);
		return true;
	}

	bool InputUInt64(const char* label, uint64_t* v, int step, int step_fast, ImGuiInputTextFlags flags) {
		// Hexadecimal input provided as a convenience but the flag name is awkward. Typically you'd use InputText() to parse your own data, if you want to handle prefixes.
		const char* format = (flags & ImGuiInputTextFlags_CharsHexadecimal) ? "%08X" : "%d";
		return ImGui::InputScalar(label, ImGuiDataType_U64, (void*)v, (void*)(step > 0 ? &step : NULL), (void*)(step_fast > 0 ? &step_fast : NULL), format, flags);
	}

	bool Spinner(const char* label, float radius, float thickness, const ImU32& color) {
		ImGuiWindow* window = ImGui::GetCurrentWindow();
		if (window->SkipItems)
			return false;

		ImGuiContext& g = *GImGui;
		const ImGuiStyle& style = g.Style;
		const ImGuiID id = window->GetID(label);

		ImVec2 pos = window->DC.CursorPos;
		ImVec2 size((radius) * 2, (radius + style.FramePadding.y) * 2);

		const ImRect bb(pos, ImVec2(pos.x + size.x, pos.y + size.y));
		ImGui::ItemSize(bb, style.FramePadding.y);
		if (!ImGui::ItemAdd(bb, id))
			return false;

		// Render
		window->DrawList->PathClear();

		int num_segments = 15;
		int start = abs((int)(ImSin(g.Time * 1.8f) * (num_segments - 5)));
		const float a_min = IM_PI * 2.0f * ((float)start) / (float)num_segments;
		const float a_max = IM_PI * 2.0f * ((float)num_segments - 3) / (float)num_segments;

		const ImVec2 centre = ImVec2(pos.x + radius, pos.y + radius + style.FramePadding.y);

		for (int i = 0; i < num_segments; i++) {
			const float a = a_min + ((float)i / (float)num_segments) * (a_max - a_min);
			window->DrawList->PathLineTo(ImVec2(centre.x + ImCos(a + g.Time * 8) * radius,
				centre.y + ImSin(a + g.Time * 8) * radius));
		}
		window->DrawList->PathStroke(color, false, thickness);
		return true;
	}

	float SeperatorHeight() {
		return ImGui::GetStyle().ItemSpacing.y; // just a spacing
	}
}
