#pragma once

#include <string>
#include <stdexcept>
#include <memory>

using std::string;
using std::runtime_error;
using std::unique_ptr;

template<typename ... Args>
static string string_format(const string& format, Args ... args) {
	int size = snprintf(nullptr, 0, format.c_str(), args ...) + 1; // extra space for '\0'
	if (size <= 0) {
		throw runtime_error("Error during formatting.");
	}
	unique_ptr<char[]> buf(new char[size]);
	snprintf(buf.get(), size, format.c_str(), args ...);
	return string(buf.get(), buf.get() + size - 1); // cut the '\0' of
}
