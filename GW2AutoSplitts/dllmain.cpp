// dllmain.cpp : Definiert den Einstiegspunkt für die DLL-Anwendung.
#include <Windows.h>
#include <stdint.h>
#include <stdio.h>
#include <memory>
#include "GW2AutoSplits.h"
#include "arcdps.h"
#include "dllmain.h"
#include "imgui/imgui.h"
#include "logger.h"

#define DLL_EXPORT extern "C" __declspec(dllexport)

using std::exception;
using std::make_unique;

/* dll attach -- from winapi */
void dll_init(HANDLE hModule) {
	return;
}

/* dll detach -- from winapi */
void dll_exit() {
	return;
}

/* dll main from winapi */
BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved) {
	switch (ul_reason_for_call) {
	case DLL_PROCESS_ATTACH:
		dll_init(hModule);
		break;
	case DLL_PROCESS_DETACH:
		dll_exit();
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	}
	return TRUE;
}


DLL_EXPORT void* get_init_addr(char* arcversionstr, ImGuiContext* imguicontext, void* id3dd9, HMODULE  arcdll, void* mallocfn, void* freefn) {
	initArcExports(arcdll);
	WriteArcLogFile("Loading GW2AutoSplits ...\n");
	try {
		Logger::init("addons\\gw2AutoSplits\\log.txt");
	} catch (exception e) {
		WriteArcLogFile("exception writing log file\n");
		WriteArcLogFile(const_cast<char*>(e.what()));
	}
	ImGui::SetCurrentContext((ImGuiContext*)imguicontext);
	ImGui::SetAllocatorFunctions((void* (*)(size_t, void*))mallocfn, (void (*)(void*, void*))freefn); // on imgui 1.80+
	GW2AutoSplits::gw2autoSplits = make_unique<GW2AutoSplits>(arcversionstr); // todo fix this
	return GW2AutoSplits::s_init;
}


DLL_EXPORT void* get_release_addr() {
	WriteArcLogFile("unloading GW2AutoSplits ...\n");
	return GW2AutoSplits::s_release;
}
