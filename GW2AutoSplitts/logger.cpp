#pragma once
#include <string>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include "arcdps.h"
#include "logger.h"
#include <mutex>
#include <time.h>

Logger* Logger::mLogger = NULL;

void Logger::e(const string& message) {
	if(mLogger) mLogger->me(message);
}
void Logger::w(const string& message) {
	if(mLogger) mLogger->mw(message);
}
void Logger::i(const string& message) {
	if(mLogger) mLogger->mi(message);
}
void Logger::d(const string& message) {
	if(mLogger) mLogger->md(message);
}
void Logger::init(const string& logfilename) {
	mLogger = new Logger(logfilename);
}

void Logger::free() {
	delete mLogger;
	mLogger = NULL;
}

Logger::Logger(const string& logFilename_) : logFilename(logFilename_){
	//std::unique_lock<std::mutex> lock1(fileMutex, std::defer_lock);
	ofstream file;
	file.open(logFilename, std::ios::out | std::ios::trunc);
	file << "GW2AutoSplitsLogFile\n";
	file.flush();
}

void Logger::me(const string& msg) {
	write("[ERR] ", msg , "\n");
}
void Logger::mw(const string& msg) {
	write("[WRN] ", msg, "\n");
}
void Logger::mi(const string& msg) {
	write("[INF] ", msg, "\n");
}
void Logger::md(const string& msg) {
	write("[DBG] ", msg, "\n");
}

void Logger::write(const string& msg0) {
	write(msg0, "", "");
}

void Logger::write(const string& msg0, const string& msg1) {
	write(msg0, msg1, "");
}

void Logger::write(const string& msg0, const string& msg1, const string& msg2) {
	//lock1.lock();
	ofstream file;
	file.open(logFilename, std::ios::out | std::ios::app);
	if (!is_valid(file)) return;
	time_t now = time(NULL);
	char buff[32];
	ctime_s(buff, sizeof(buff), &now);
	// strip \n
	for (int i = 0; i < sizeof(buff); i++) {
		if (buff[i] == '\n') {
			buff[i] = '\0';
			break;
		}
	}
	file << buff;
	file << msg0;
	file << msg1;
	file << msg2;
	file.flush();
	file.close();
	//lock1.unlock();
}

bool Logger::is_valid(const ofstream& logFile) {
	return logFile.is_open();
}
