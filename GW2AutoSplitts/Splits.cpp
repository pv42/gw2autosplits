#include <fstream>
#include <string>

#include "json.hpp"
#include "Splits.h"
#include "arcdps.h"
#include "string_format.hpp"
#include "GW2.h"
#include "logger.h"

using std::unique_lock;

using nlohmann::json;
using std::ifstream;
using std::exception;
using std::make_shared;

const static int DEFAULT_LWR_Y = -10000;
const static int DEFAULT_UPR_Y = 20000;

constexpr auto KEY_LWR_X = "lwr_x";
constexpr auto KEY_LWR_Y = "lwr_y";
constexpr auto KEY_LWR_Z = "lwr_z";
constexpr auto KEY_UPR_X = "upr_x";
constexpr auto KEY_UPR_Y = "upr_y";
constexpr auto KEY_UPR_Z = "upr_z";

constexpr auto KEY_MID_X = "mid_x";
constexpr auto KEY_MID_Y = "mid_y";
constexpr auto KEY_MID_Z = "mid_z";
constexpr auto KEY_RADIUS = "radius";

constexpr auto KEY_MAP_ID = "map_id";
constexpr auto KEY_VERSION = "version";
constexpr auto KEY_NAME = "name";
constexpr auto KEY_TYPE = "type";
constexpr auto KEY_CHECKPOINT = "checkpoint";
constexpr auto KEY_CONDITION = "conditions";
constexpr auto KEY_CHECKPOINT_CONDITION = "checkpointConditions";
constexpr auto KEY_OTHER_MAPS = "otherMapIds";
constexpr auto KEY_SPLITS = "splits";


mutex ActiveSplitSet::updateMutex;

Split::Split(vector<SplitCondition_PTR> conditions_, string name_) : conditions(conditions_), name(name_), checkpoint(false) {
	bestRun = (milliseconds)0;
	bestSegment = (milliseconds)0;
}
Split::Split(const vector<SplitCondition_PTR> conditions_, const vector<SplitCondition_PTR> checkpointConditions_, string name_, milliseconds bestRun_, milliseconds bestSegment_, bool checkpoint_) :
	conditions(conditions_),
	name(name_),
	bestRun(bestRun_),
	bestSegment(bestSegment_),
	checkpoint(checkpoint_),
	checkpointConditions(checkpointConditions_) {
}

Split::Split() : name(""), bestRun(0), bestSegment(0), checkpoint(false) {} // empty init


Split::Split(Split* other) :
	name(other->name),
	bestRun(other->bestRun),
	bestSegment(other->bestSegment),
	conditions(other->conditions),
	checkpointConditions(other->checkpointConditions),
	checkpoint(other->checkpoint) {}

bool Split::isSatisfied(float* pos, arcdps_event* ev, ourclock::time_point* time) const {
	for (SplitCondition_PTR condition : conditions) {
		if (condition->isSatisfied(pos, ev)) {
			*time = condition->getSplitTime();
			return true;
		}
	}
	return false;
}

bool Split::isGoToCheckpoint(float* pos, arcdps_event* ev) const {
	for (SplitCondition_PTR condition : checkpointConditions) {
		if (condition->isSatisfied(pos, ev)) {
			return true;
		}
	}
	return false;
}

const vector<SplitCondition_PTR> Split::getConditions() const {
	return conditions;
}

const vector<SplitCondition_PTR> Split::getCheckpointConditions() const {
	return checkpointConditions;
}

vector<SplitCondition_PTR>& Split::getConditionsEditable() {
	return conditions;
}

vector<SplitCondition_PTR>& Split::getCheckpointConditionsEditable() {
	return checkpointConditions;
}

const bool Split::hasBestRun() const {
	return bestRun != (milliseconds)0;
}

const milliseconds Split::getBestRun() const {
	return bestRun;
}

void Split::updateBestRun(milliseconds time) {
	if (time.count() > 0) {
		Logger::d(string_format("[SPLIT] best run time for %s is now %dms (old %dms)", name.c_str(), time.count(), bestRun));
		bestRun = time;
	}
}

const bool Split::hasBestSegment() const {
	return bestSegment != (milliseconds)0;
}

const milliseconds Split::getBestSegment() const {
	return bestSegment;
}

void Split::updateBestSegment(milliseconds segmentTime) {
	if (segmentTime.count() > 0 && (segmentTime < bestSegment || bestSegment.count() == 0)) {
		bestSegment = segmentTime;
		Logger::d(string_format("[SPLIT] best seg time for %s is now %dms", name.c_str(), segmentTime.count()));
	}
}

void Split::deleteBestTimes() {
	bestRun = (milliseconds)0;
	bestSegment = (milliseconds)0;
}

SplitSet::SplitSet(string filepath_, const string name_, int mapId_, set<int> otherMapIds_, vector<Split> splits_):
	name(name_), mapId(mapId_), splits(splits_), otherMapIds(otherMapIds_), filepath(filepath_) {}

SplitSet::SplitSet(const SplitSet* set) :
	name(set->name), mapId(set->mapId), splits(set->splits), otherMapIds(set->otherMapIds), filepath(set->filepath) {
	Logger::d(string_format("[SPLIT] created ss with cpy constructor to path %s", filepath.c_str()));
}

bool checkHasString(const json& j, const string& key) {
	return j.contains(key) && j[key].is_string();
}

bool checkHasInt(const json& j, const string& key) {
	return j.contains(key) && j[key].is_number_integer();
}

bool checkHasNumber(const json& j, const string& key) {
	return j.contains(key) && j[key].is_number();
}

SplitCondition_PTR readSplitCondition(const json j_condition) {
	if (!checkHasString(j_condition, KEY_TYPE)) throw exception("condition is missing type");
	string type = j_condition[KEY_TYPE];
	if (type == "position") {
		if (!j_condition.contains(KEY_LWR_X) || !j_condition[KEY_LWR_X].is_number() ||
			!j_condition.contains(KEY_LWR_Z) || !j_condition[KEY_LWR_Z].is_number() ||
			!j_condition.contains(KEY_UPR_X) || !j_condition[KEY_UPR_X].is_number() ||
			!j_condition.contains(KEY_UPR_Z) || !j_condition[KEY_UPR_Z].is_number())
			throw exception("position condition is missing coordinate or not a number");
		float lwr_x = j_condition[KEY_LWR_X];
		float lwr_z = j_condition[KEY_LWR_Z];
		float upr_x = j_condition[KEY_UPR_X];
		float upr_z = j_condition[KEY_UPR_Z];
		float lwr_y, upr_y;
		if (j_condition.contains(KEY_LWR_Y)) {
			if (!j_condition[KEY_LWR_Y].is_number()) throw exception("position condition coordinate not a number");
			lwr_y = j_condition[KEY_LWR_Y];
		} else {
			lwr_y = DEFAULT_LWR_Y;
		}
		if (j_condition.contains(KEY_UPR_Y)) {
			if (!j_condition[KEY_UPR_Y].is_number()) throw exception("position condition coordinate not a number");
			upr_y = j_condition[KEY_UPR_Y];
		} else {
			upr_y = DEFAULT_UPR_Y;
		}
		return (SplitCondition_PTR)make_shared<BoxPositionalSplitCondition>(lwr_x, lwr_y, lwr_z, upr_x, upr_y, upr_z);
	} else if (type == "sphere") {
		if (!j_condition.contains(KEY_MID_X)) throw exception("middle x pos is missing");
		if (!j_condition.contains(KEY_MID_Y)) throw exception("middle y pos is missing");
		if (!j_condition.contains(KEY_MID_Z)) throw exception("middle z pos is missing");
		if (!j_condition.contains(KEY_RADIUS)) throw exception("radius is missing");
		float x = j_condition[KEY_MID_X];
		float y = j_condition[KEY_MID_Y];
		float z = j_condition[KEY_MID_Z];
		float r = j_condition[KEY_RADIUS];
		return (SplitCondition_PTR)make_shared<SphericalPositionalSplitCondition>(x, y, z, r);
	} else if (type == "combat") {
		if (!j_condition.contains("target")) throw exception("combat condition is missing target");
		string target = j_condition["target"];
		return (SplitCondition_PTR)make_shared<CombatSplitCondition>(target);
	} else if (type == "leftCombat") {
		if (!j_condition.contains("target")) throw exception("left combat condition is missing target");
		string target = j_condition["target"];
		return (SplitCondition_PTR)make_shared<LeftCombatSplitCondition>(target);
	} else if (type == "logEnd") {
		if(j_condition.contains("species_id")) return (SplitCondition_PTR)make_shared<LogEndSplitCondition>(j_condition["species_id"]);
		else return (SplitCondition_PTR)make_shared<LogEndSplitCondition>();
	} else if (type == "logStart") {
		if (j_condition.contains("species_id")) return (SplitCondition_PTR)make_shared<LogStartSplitCondition>(j_condition["species_id"]);
		else return (SplitCondition_PTR)make_shared<LogStartSplitCondition>();
	} else {
		throw exception(string_format("%s is no valid split condition type", type.c_str()).c_str());
	}
}

shared_ptr<SplitSet> SplitSet::loadFromFile(const string& fileName) {
	Logger::i(string_format("[FILE IO] loading split file: %s", fileName.c_str()));
	ifstream file(fileName);
	json j;
	file >> j;
	if (!checkHasInt(j, KEY_VERSION)) throw exception("splitset is missing version");
	int version = j[KEY_VERSION];
	string key_splits = version > 5 ? KEY_SPLITS : "splitts"; // prior to version 6 there was a type all over the project ...
	if(version != 7) Logger::d(string_format("[FILE IO] version is %d, current is 7", version));
	if (version == 1 || version == 2 || version == 3 ||version == 4 || version == 5 || version == 6 ||version == 7) {
		if (!checkHasString(j, KEY_NAME)) throw exception("splitset is missing valid name");
		if (!checkHasInt(j, KEY_MAP_ID)) throw exception("splitset is missing valid map_id");
		int map = j[KEY_MAP_ID];
		string name = j[KEY_NAME];
		Logger::d(string_format("[FILE IO] loading split %s for map %d", name.c_str(), map));
		auto& j_splits = j[key_splits];
		vector<Split> splits;
		for (auto& j_split : j_splits) {
			if (!checkHasString(j_split, KEY_NAME)) throw exception("split is missing name");
			string splitName = j_split[KEY_NAME];
			bool checkpoint = false;
			if (version >= 3 && j_split.contains(KEY_CHECKPOINT)) {
				if (!j_split[KEY_CHECKPOINT].is_boolean()) throw exception("checkpoint is type not boolean");
				checkpoint = j_split[KEY_CHECKPOINT];
			}
			auto& j_conditions = j_split[KEY_CONDITION];
			vector<SplitCondition_PTR> conditions;
			for (auto& j_condition : j_conditions) {
				conditions.push_back(readSplitCondition(j_condition));
			}
			vector<SplitCondition_PTR> checkpointConditions;
			if(j_split.contains(KEY_CHECKPOINT_CONDITION)) {
				auto& j_checkpoint_conditions = j_split[KEY_CHECKPOINT_CONDITION];
				
				for (auto& j_condition : j_checkpoint_conditions) {
					checkpointConditions.push_back(readSplitCondition(j_condition));
				}
			}
			if (version >= 2) {
				if (j_split.contains("times")) {
					if (!j_split["times"].contains("best_run")) throw exception("times is missing best_run");
					if (!j_split["times"].contains("best_segment")) throw exception("split is missing best_segment");
					long best_run = j_split["times"]["best_run"];
					long best_segment = j_split["times"]["best_segment"];
					Split split(conditions, checkpointConditions, splitName, (milliseconds)best_run, (milliseconds)best_segment, checkpoint);
					splits.push_back(split);
				} else {
					Logger::w("[FILE IO] split is missing times");
					Split split(conditions, checkpointConditions, splitName, (milliseconds)0, (milliseconds)0, checkpoint);
					splits.push_back(split);
				}
			} else if(version == 1) { 
				Split split(conditions, splitName);
				splits.push_back(split);
			} 
		}
		set<int> otherMaps;
		if(version >= 5) {
			vector<int> otherIds;
			for(int id: j[KEY_OTHER_MAPS]) {
				otherMaps.insert(id);
			}
		}
		if (version == 3) { // correct unsplitss conditions
			Logger::d("[FILE IO] modifing unsplit conditions for version 3");
			for (int i = 0; i < splits.size() - 1; i++) {
				splits[i] = Split(
					splits[i].getConditions(),
					splits[i + 1].getCheckpointConditions(),
					splits[i].name,
					splits[i].getBestRun(),
					splits[i].getBestSegment(),
					splits[i].checkpoint);
			}
		}
		return make_shared<SplitSet>(fileName, name, map, otherMaps, splits);
	} else {
		throw exception("Version not supported.");
	}
}

json getJsonFromCondition(SplitCondition_PTR condition) {
	json j_con;
	j_con[KEY_TYPE] = condition->getType();
	if (condition->getType() == "position") {
		BoxPositionalSplitCondition_PTR boxCondition = reinterpret_pointer_cast<BoxPositionalSplitCondition>(condition);
		j_con[KEY_LWR_X] = boxCondition->lwr[0];
		j_con[KEY_LWR_Z] = boxCondition->lwr[2];
		j_con[KEY_UPR_X] = boxCondition->upr[0];
		j_con[KEY_UPR_Z] = boxCondition->upr[2];
		if (boxCondition->lwr[1] != DEFAULT_LWR_Y) j_con[KEY_LWR_Y] = boxCondition->lwr[1];
		if (boxCondition->upr[1] != DEFAULT_UPR_Y) j_con[KEY_UPR_Y] = boxCondition->upr[1];
	} else if (condition->getType() == "sphere") {
		SphericalPositionalSplitCondition_PTR sphericalCondition = reinterpret_pointer_cast<SphericalPositionalSplitCondition>(condition);
		j_con[KEY_MID_X] = sphericalCondition->mid[0];
		j_con[KEY_MID_Y] = sphericalCondition->mid[1];
		j_con[KEY_MID_Z] = sphericalCondition->mid[2];
		j_con[KEY_RADIUS] = sphericalCondition->radius;
	} else if (condition->getType() == "combat") {
		CombatSplitCondition_PRT combatCondition = reinterpret_pointer_cast<CombatSplitCondition>(condition);
		j_con["target"] = combatCondition->target;
	} else if (condition->getType() == "leftCombat") {
		LeftCombatSplitCondition_PTR lCombatCondition = reinterpret_pointer_cast<LeftCombatSplitCondition>(condition);
		j_con["target"] = lCombatCondition->target;
	} else if (condition->getType() == "buffApplied") {
		BuffAppliedSplitCondition_PTR buffCondition = reinterpret_pointer_cast<BuffAppliedSplitCondition>(condition);
		j_con["target"] = buffCondition->target;
		j_con["buff"] = buffCondition->buff;
	} else if (condition->getType() == "logStart") {
		if (reinterpret_pointer_cast<LogStartSplitCondition>(condition)->species_id > 0) {
			j_con["species_id"] = reinterpret_pointer_cast<LogStartSplitCondition>(condition)->species_id;
		}
	} else if (condition->getType() == "logEnd") {
		if (reinterpret_pointer_cast<LogEndSplitCondition>(condition)->species_id > 0) {
			j_con["species_id"] = reinterpret_pointer_cast<LogEndSplitCondition>(condition)->species_id;
		}
	} else {
		Logger::w(string_format("[FILE IO] invalid split type: %s\n", condition->getType().c_str()));
	}
	return j_con;
}

void SplitSet::writeToFile() {
	try {
		Logger::d(string_format("[FILE IO] writing to: %s", filepath.c_str()));
		ofstream file(filepath);
		json j;
		j[KEY_VERSION] = 7;
		j[KEY_NAME] = this->name;
		j[KEY_MAP_ID] = this->mapId;
		j[KEY_OTHER_MAPS] = vector<json>();
		for (int id: this->otherMapIds) {
			j[KEY_OTHER_MAPS].push_back(id);
		}
		j[KEY_SPLITS] = vector<json>();
		Logger::d(string_format("[FILE IO] writing version: %d (%d splits)", 2, this->splits.size()));
		int split_index = 0;
		for (Split split : this->splits) {
			Logger::d(string_format("[FILE IO] writing split: %s", split.name.c_str()));
			json j_sp;
			j_sp[KEY_NAME] = split.name;
			if(split.checkpoint) j_sp[KEY_CHECKPOINT] = split.checkpoint;
			j_sp[KEY_CONDITION] = vector<json>();
			for (SplitCondition_PTR condition : split.getConditions()) {
				Logger::d(string_format("[FILE IO] writing condition of type: %s", condition->getType().c_str()));
				json j_con = getJsonFromCondition(condition);
				j_sp[KEY_CONDITION].push_back(j_con);
			}
			if(split.getCheckpointConditions().size() > 0) {
				j_sp[KEY_CHECKPOINT_CONDITION] = vector<json>();
				for (SplitCondition_PTR condition : split.getCheckpointConditions()) {
					Logger::d(string_format("[FILE IO] writing condition of type: %s", condition->getType().c_str()));
					json j_con = getJsonFromCondition(condition);
					j_sp[KEY_CHECKPOINT_CONDITION].push_back(j_con);
				}
			}
			Logger::d("[FILE IO] writing times");
			j_sp["times"] = {
					{"best_run", split.getBestRun().count()},
					{"best_segment", split.getBestSegment().count()}
			};
			j[KEY_SPLITS].push_back(j_sp);
			split_index++;
		}
		Logger::d("[FILE IO] writing done");
		file << j.dump(2);
		file.flush();
		file.close();
	} catch (exception e) {
		Logger::w("[FILE IO] exception when trying to write splitfile:");
		Logger::w(e.what());
	}
}

void SplitSet::deleteBestTimes() {
	for (Split split : splits) {
		split.deleteBestTimes();
	}
}

ActiveSplitSet::ActiveSplitSet() : SplitSet(string(), string(), 0, set<int>(), vector<Split>()), current_split(0) {
	Logger::d("[ACTIVE SET] created empty active sps");
}

ActiveSplitSet::ActiveSplitSet(const shared_ptr<SplitSet> set) : SplitSet(set.get()), current_split(0) {
	Logger::d("[ACTIVE SET] created active sps from sps " + set->getName());
	startTime = ourclock::now(); //
}

void ActiveSplitSet::skip() {
	times.push_back((milliseconds)0);
	current_split++;
}

void ActiveSplitSet::unsplit() {
	if (times.size() == 0) return;
	times.pop_back();
	current_split--;
}

bool ActiveSplitSet::isPersonalBest() {
	return times.size() == splits.size() && (times[times.size() - 1] < splits[splits.size() - 1].getBestRun() || splits[splits.size() - 1].getBestRun() == (milliseconds)0); // pb if full and last split is better
}

void ActiveSplitSet::saveTimes() {
	bool modified = false;
	bool isPB = isPersonalBest();
	for (int i = 0; i < times.size(); i++) {
		if (isPB) { // only update best times for new pb
			modified = true;
			splits[i].updateBestRun(times[i]);
			Logger::d(string_format("[ACTIVE SET] updated times: run=%d" ,times[i].count()));
		}
		milliseconds segmentTime = getSegmentTime(i);
		if (segmentTime.count() > 0) {
			splits[i].updateBestSegment(segmentTime);
			Logger::d(string_format("[ACTIVE SET] updated times: seg=%d", segmentTime.count()));
			modified = true;
		}
	}
	if (modified) this->writeToFile();
	modified = false;
}

void ActiveSplitSet::reset() {
	current_split = 0;
	times.clear();
	startTime = ourclock::now();
}

milliseconds ActiveSplitSet::getSegmentTime(unsigned int splitIndex) const {
	if (times.size() <= splitIndex) return (milliseconds)0; // not yet split
	if (times[splitIndex] == (milliseconds)0) return  (milliseconds)0; // current split skipped
	if (splitIndex == 0) return times[0]; // segment time for 1st split is just time
	if (times[splitIndex - 1] == (milliseconds)0) return (milliseconds)0; // last split skipped
	return times[splitIndex] - times[splitIndex - 1];
}

const Split* ActiveSplitSet::getCurrentSplit() const {
	if (current_split >= splits.size()) return NULL;
	return &splits[current_split];
}

const Split* ActiveSplitSet::getPrevSplit() const {
	if (current_split-1 >= splits.size()) return NULL;
	if (current_split <= 1 ) return NULL;
	return &splits[current_split - 1];
}

int ActiveSplitSet::getCurrentSplitIndex() const {
	return current_split;
}

const vector<Split> SplitSet::getSplits() const {
	return splits;
}

vector<Split>& SplitSet::getSplitsEditable() {
	return splits;
}

const string SplitSet::getName() const {
	return name;
} 

void SplitSet::setName(const string& newName) {
	name = string(newName);
}

const bool SplitSet::hasMapId(const int mapId_) const {
	if(mapId_ == mapId) return true;
	return otherMapIds.contains(mapId_);
}

void ActiveSplitSet::split(milliseconds time) {
	if (splits.size() > current_split) {
		current_split++;
		times.push_back(time);
		Logger::i(string_format("[ACTIVE SET] SPLIT! %d", current_split));
	}
}

void ActiveSplitSet::update(float* pos, arcdps_event* ev, ourclock::time_point time) {
	unique_lock<mutex> lock(updateMutex);
	const Split* currentSplit = getCurrentSplit();
	ourclock::time_point spTime;
	if (currentSplit != NULL && currentSplit->isSatisfied(pos, ev, &spTime)) {
		split(std::chrono::duration_cast<milliseconds>(spTime - startTime));
	}
	const Split* prevSplit = getPrevSplit();
	if (prevSplit != NULL && prevSplit->isGoToCheckpoint(pos, ev)) {
		while (0 < current_split && !splits[current_split-1].checkpoint) {
			current_split--;
			times.pop_back();
			Logger::d(string_format("[ACTIVE SET] UNSPLIT! %d", current_split));
		}
	}
}

const ourclock::time_point ActiveSplitSet::getStartTime() const {
	return startTime;
}

const vector<milliseconds> ActiveSplitSet::getTimes() const {
	return times;
}

const string SplitCondition::getType() const {
	return type;
}

const ourclock::time_point SplitCondition::getSplitTime() const {
	return ourclock::now();
}

const ourclock::time_point ArcSplitCondition::getSplitTime() const {
	return splitTime;
}

void ArcSplitCondition::setSplitTime(uint64_t tgTime) {
	splitTime = Timing::timeGetTimeToChrono(tgTime);
}

BoxPositionalSplitCondition::BoxPositionalSplitCondition(float lwr_x, float lwr_y, float lwr_z, float upr_x, float upr_y, float upr_z) {
	lwr[0] = lwr_x;
	lwr[1] = lwr_y;
	lwr[2] = lwr_z;
	upr[0] = upr_x;
	upr[1] = upr_y;
	upr[2] = upr_z;
	type = "position";
}

BoxPositionalSplitCondition::BoxPositionalSplitCondition(float lwr_x, float lwr_z, float upr_x, float upr_z) {
	lwr[0] = lwr_x;
	lwr[1] = DEFAULT_LWR_Y;
	lwr[2] = lwr_z;
	upr[0] = upr_x;
	upr[1] = DEFAULT_UPR_Y;
	upr[2] = upr_z;
	type = "position";
}

bool BoxPositionalSplitCondition::isSatisfied(float* pos, arcdps_event* ev) {
	for (int i = 0; i < 3; i++) {
		if (pos[i] < lwr[i] || pos[i] > upr[i]) return false;
	}
	return true;
}

SphericalPositionalSplitCondition::SphericalPositionalSplitCondition(const float x, const float y, const float z, const float radius) {
	mid[0] = x;
	mid[1] = y;
	mid[2] = z;
	this->radius = radius;
	type = "sphere";
}

float dist3dSq(const float* a, const float* b) { // distance 3 dimenisional squared
	float dist = 0;
	for (int i = 0; i < 3; i++) {
		float d = a[i] - b[i];
		dist += d * d;
	}
	return dist;
}

bool SphericalPositionalSplitCondition::isSatisfied(float* pos, arcdps_event* ev) {
	return radius * radius > dist3dSq(pos, mid);
}

BuffAppliedSplitCondition::BuffAppliedSplitCondition(const string& target_, const SKILL_ID buff_) : target(target_), buff(buff_) {
	type = "buffApplied";
}

bool BuffAppliedSplitCondition::isSatisfied(float* pos, arcdps_event* ev) {
	if (!ev) return false;
	cbtevent* event = ev->event;
	ag* src = ev->src;
	ag* dst = ev->dst;
	if (!event || !src || !dst) return false;
	bool ret = event->buff && event->skillid == buff && !target.compare(src->name) && !target.compare(dst->name);
	if (ret) setSplitTime(event->time);
	return ret;
}

CombatSplitCondition::CombatSplitCondition(const string& target_) : target(target_) {
	type = "combat";
}

bool CombatSplitCondition::isSatisfied(float* pos, arcdps_event* ev) {
	if (!ev) return false; 
	ag* src = ev->src;
	ag* dst = ev->dst;
	bool ret =  (dst && dst->name && strlen(dst->name) && !target.compare(dst->name)) || (src && src->name && strlen(src->name) && !target.compare(src->name));
	if (ret) {
		if (ev->event) {
			setSplitTime(ev->event->time);
		} else {
			Logger::w("[SPLIT] Combat ev has no event");
		}
	}
	return ret;
}

LeftCombatSplitCondition::LeftCombatSplitCondition(const string& target_) : target(target_) {
	type = "leftCombat";
}

bool LeftCombatSplitCondition::isSatisfied(float* pos, arcdps_event* ev) {
	if (!ev) return false;
	cbtevent* event = ev->event;
	ag* src = ev->src;
	if (!event || !src) return false;
	bool ret = event->is_statechange == CBTS_EXITCOMBAT && !target.compare(src->name);
	if(ret) setSplitTime(event->time);
	return ret;
}

LogEndSplitCondition::LogEndSplitCondition() {
	type = "logEnd";
	species_id = 0;
}

LogEndSplitCondition::LogEndSplitCondition(uint64_t species_id) {
	type = "logEnd";
	this->species_id = species_id;
}

bool LogEndSplitCondition::isSatisfied(float* pos, arcdps_event* ev) {
	if (!ev) return false;
	cbtevent* event = ev->event;
	ag* src = ev->src;
	if (!event) return false;
	bool ret = event->is_statechange == CBTS_LOGEND && (!species_id || species_id == event->src_agent);
	if (ret) setSplitTime(event->time);
	return ret;
}

LogStartSplitCondition::LogStartSplitCondition() {
	type = "logStart";
	species_id = 0;
}

LogStartSplitCondition::LogStartSplitCondition(uint64_t species_id) {
	type = "logStart";
	this->species_id = species_id;
}

bool LogStartSplitCondition::isSatisfied(float* pos, arcdps_event* ev) {
	if (!ev) return false;
	cbtevent* event = ev->event;
	ag* src = ev->src;
	if (!event) return false;
	bool ret = event->is_statechange == CBTS_LOGSTART && (!species_id || species_id == event->src_agent);
	if(ret) setSplitTime(event->time);
	return ret;
}

